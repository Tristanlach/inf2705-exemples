#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint prog1;
GLint loc1Vertex;
GLint loc1Color;
GLint loc1matrModel;
GLint loc1matrVisu;
GLint loc1matrProj;
GLint loc1TessLevelInner;
GLint loc1TessLevelOuter;
GLuint prog2;
GLint loc2Vertex;
GLint loc2Color;
GLint loc2matrModel;
GLint loc2matrVisu;
GLint loc2matrProj;
GLint loc2TessLevelInner;
GLint loc2TessLevelOuter;

GLuint vao[1];   // un VAO
GLuint vbo[1];   // un VBO

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
