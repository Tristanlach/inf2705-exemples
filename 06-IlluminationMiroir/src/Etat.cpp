#include "Etat.h"

SINGLETON_DECLARATION_CPP(Etat);

float Etat::xrot = 0.0;
float Etat::sens = +1.0;
bool Etat::enmouvement = true;
bool Etat::afficheAxes = true;
bool Etat::avecMurs = true;
bool Etat::avecTheiere = true;
bool Etat::avecClip = true;
bool Etat::avecLimite = true;
bool Etat::afficheNormales = false;
float Etat::miroirX = -2.0;
glm::ivec2   Etat::sourisPosPrec = glm::ivec2(0);
