#ifndef __ETAT_H__
#define __ETAT_H__

#include <glm/glm.hpp>
#include "inf2705-Singleton.h"

//
// variables d'état
//
class Etat : public Singleton<Etat>
{
    SINGLETON_DECLARATION_CLASSE(Etat);
public:
    static float xrot;
    static float sens;
    static bool enmouvement;  // le modèle est en mouvement/rotation automatique ou non
    static bool afficheAxes; // indique si on affiche les axes
    static bool avecMurs;
    static bool avecTheiere;
    static bool avecClip;
    static bool avecLimite;
    static bool afficheNormales;
    static float miroirX;
    static glm::ivec2 sourisPosPrec;  // la position de la souris
};

#endif
