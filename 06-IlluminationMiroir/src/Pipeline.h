#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint prog = -1;
GLint locVertex = -1;
GLint locNormal = -1;
GLint locColor = -1;
GLint locmatrModel = -1;
GLint locmatrVisu = -1;
GLint locmatrProj = -1;
GLint locmatrNormale = -1;
GLint locavecClip = -1;
GLint locplanCoupe = -1;
GLint locafficheNormales = -1;
GLuint indLightSource = -1;
GLuint indFrontMaterial = -1;
GLuint indLightModel = -1;
GLuint progBase = -1;  // le programme de nuanceurs de base
GLint locVertexBase = -1;
GLint locColorBase = -1;
GLint locmatrModelBase = -1;
GLint locmatrVisuBase = -1;
GLint locmatrProjBase = -1;

GLuint vao[2];
GLuint vbo[4];
GLuint ubo[3];

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
