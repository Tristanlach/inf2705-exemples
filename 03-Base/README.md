# 03-Base
<a href="images/image.png"><img width=200px src="images/image.png"></a>
<a href="images/imageEx1.png"><img width=200px src="images/imageEx1.png"></a>
<a href="images/imageEx2.png"><img width=200px src="images/imageEx2.png"></a>
<a href="images/imageEx3.png"><img width=200px src="images/imageEx3.png"></a>
<a href="images/imageEx4.png"><img width=200px src="images/imageEx4.png"></a>
<a href="images/imageEx5.png"><img width=200px src="images/imageEx5.png"></a>
<a href="images/imageEx8.png"><img width=200px src="images/imageEx8.png"></a>

Comme exercice, modifier l'affichage afin de produire l'image ci-dessous.<br>
(Dans le nuanceur de fragments...)<br>
<a href="images/imageExercice.png"><img width=200px src="images/imageExercice.png"></a>
