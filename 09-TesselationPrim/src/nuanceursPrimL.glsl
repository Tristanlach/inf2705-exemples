// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

// ==> Les variables en commentaires ci-dessous sont déclarées implicitement:
// out gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

layout(location=0) in vec4 Vertex;
layout(location=3) in vec4 Color;

uniform mat4 matrModel;

out Attribs {
    vec4 couleur;
} AttribsOut;

void main(void)
{
    gl_Position = matrModel * Vertex;
    AttribsOut.couleur = Color;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_TESSCTRL)

// ==> Les variables en commentaires ci-dessous sont déclarées implicitement:
// in int gl_PatchVerticesIn;
// in int gl_PrimitiveID;
// in int gl_InvocationID;
// in gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_in[gl_MaxPatchVertices];

// patch out float gl_TessLevelOuter[4];
// patch out float gl_TessLevelInner[2];
// out gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_out[];

layout(vertices = 4) out;

uniform float TessLevelInner;
uniform float TessLevelOuter;

in Attribs {
    vec4 couleur;
} AttribsIn[];

out Attribs {
    vec4 couleur;
} AttribsOut[];

void main()
{
    // copier la position du sommet vers la sortie
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;

    // donner les niveaux de tessellation désirée (on le fait seuleemnt pour la première invocation)
    if ( gl_InvocationID == 0 )
    {
        gl_TessLevelInner[0] = TessLevelInner;
        gl_TessLevelInner[1] = TessLevelInner;
        gl_TessLevelOuter[0] = TessLevelOuter;
        gl_TessLevelOuter[1] = TessLevelOuter;
        gl_TessLevelOuter[2] = TessLevelOuter;
        gl_TessLevelOuter[3] = TessLevelOuter;
    }

    // copier les autres attributs vers la sortie
    AttribsOut[gl_InvocationID].couleur = AttribsIn[gl_InvocationID].couleur;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_TESSEVAL)

// ==> Les variables en commentaires ci-dessous sont déclarées implicitement:
// in vec3 gl_TessCoord;
// in int gl_PatchVerticesIn;
// in int gl_PrimitiveID;
// patch in float gl_TessLevelOuter[4];
// patch in float gl_TessLevelInner[2];
// in gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_in[gl_MaxPatchVertices];

// out gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

layout(isolines) in;

in Attribs {
    vec4 couleur;
} AttribsIn[];

out Attribs {
    vec4 couleur;
} AttribsOut;

float interpole( float v0, float v1, float v2, float v3 )
{
    // mix( x, y, f ) = x * (1-f) + y * f.
    float v01 = mix( v0, v1, gl_TessCoord.x );
    float v32 = mix( v3, v2, gl_TessCoord.x );
    return mix( v01, v32, gl_TessCoord.y );
}
vec2 interpole( vec2 v0, vec2 v1, vec2 v2, vec2 v3 )
{
    // mix( x, y, f ) = x * (1-f) + y * f.
    vec2 v01 = mix( v0, v1, gl_TessCoord.x );
    vec2 v32 = mix( v3, v2, gl_TessCoord.x );
    return mix( v01, v32, gl_TessCoord.y );
}
vec3 interpole( vec3 v0, vec3 v1, vec3 v2, vec3 v3 )
{
    // mix( x, y, f ) = x * (1-f) + y * f.
    vec3 v01 = mix( v0, v1, gl_TessCoord.x );
    vec3 v32 = mix( v3, v2, gl_TessCoord.x );
    return mix( v01, v32, gl_TessCoord.y );
}
vec4 interpole( vec4 v0, vec4 v1, vec4 v2, vec4 v3 )
{
    // mix( x, y, f ) = x * (1-f) + y * f.
    vec4 v01 = mix( v0, v1, gl_TessCoord.x );
    vec4 v32 = mix( v3, v2, gl_TessCoord.x );
    return mix( v01, v32, gl_TessCoord.y );
}

void main()
{
    // interpoler la position et les attributs selon gl_TessCoord
    gl_Position = interpole( gl_in[0].gl_Position, gl_in[1].gl_Position, gl_in[2].gl_Position, gl_in[3].gl_Position );
    AttribsOut.couleur = interpole( AttribsIn[0].couleur, AttribsIn[1].couleur, AttribsIn[2].couleur, AttribsIn[3].couleur );
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_GEOMETRIE)

// ==> Les variables en commentaires ci-dessous sont déclarées implicitement:
// in gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_in[];

// out gl_PerVertex
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

layout(lines) in;
layout(line_strip, max_vertices = 2) out;

uniform mat4 matrVisu;
uniform mat4 matrProj;

in Attribs {
    vec4 couleur;
} AttribsIn[];

out Attribs {
    vec4 couleur;
} AttribsOut;

void main()
{
    // calculer le centre
    vec4 centre = vec4(0.0);
    for ( int i = 0 ; i < gl_in.length() ; ++i )
        centre += gl_in[i].gl_Position;
    centre /= gl_in.length();

    // émettre les sommets
    for ( int i = 0 ; i < gl_in.length() ; ++i )
    {
        gl_Position = matrProj * matrVisu * gl_in[i].gl_Position;
        //gl_Position = matrProj * matrVisu * mix( gl_in[i].gl_Position, centre, 0.1 );
        AttribsOut.couleur = AttribsIn[i].couleur;
        EmitVertex();
    }
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

// ==> Les variables en commentaires ci-dessous sont déclarées implicitement:
// in vec4 gl_FragCoord;
// in bool gl_FrontFacing;
// in vec2 gl_PointCoord;
// out float gl_FragDepth;

in Attribs {
    vec4 couleur;
} AttribsIn;

// out float gl_FragDepth;
out vec4 FragColor;

void main(void)
{
    FragColor = gl_FrontFacing ? AttribsIn.couleur : vec4( 1.0 - AttribsIn.couleur.rgb, AttribsIn.couleur.a );
    //FragColor = AttribsIn.couleur;
}

#endif
