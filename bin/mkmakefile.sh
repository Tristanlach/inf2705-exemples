#!/bin/bash

domakefile()
{
cat <<EOF
CONTEXT=sdl2
#CONTEXT=glfw3

CXXFLAGS += -DFENETRE_\$(CONTEXT)
CXXFLAGS += -g -W -Wall -Wno-unused-parameter -Wvla -std=c++17 -pedantic # -Wno-deprecated-declarations
CXXFLAGS += \$(shell pkg-config --cflags glew || echo -I/usr/local/include)
CXXFLAGS += \$(shell pkg-config --cflags \$(CONTEXT) || echo -I/usr/local/include/SDL2)

LDFLAGS += -g
LDFLAGS += \$(shell pkg-config --libs glew || echo -I/usr/local/lib -lGLEW)
LDFLAGS += \$(shell pkg-config --libs \$(CONTEXT) || echo -I/usr/local/lib -lSDL2)
LDFLAGS += -lfreeimage

ifeq "\$(shell uname)" "Darwin"
  LDFLAGS += -framework OpenGL
  ifeq "\$(CONTEXT)" "glfw3"
    LDFLAGS += -lobjc -framework Foundation -framework Cocoa
  endif
endif

SRC=main
BUILD=./build-\$(shell uname)
EXE=\$(BUILD)/$2.exe

exe : \$(EXE)
run : exe
	\$(EXE)
\$(EXE) : *.cpp *.h
	mkdir -p \$(BUILD)
	\$(CXX) \$(CXXFLAGS) -o\$@ *.cpp \$(LDFLAGS)

# pour construire un projet en utilisant cmake (il faut ensuite aller dans '\$(BUILD)' et y faire make)
cmake :
	mkdir -p \$(BUILD)
	cd \$(BUILD) && cmake ..

# nettoyage
clean :
	rm -rf \$(BUILD)/
EOF
}

domakefileremise()
{
TEXTURES=`\ls -d textures 2>/dev/null`
cat <<EOF

# pour créer le fichier à remettre dans Moodle
remise zip :
	make clean
	rm -f INF2705_remise_$1.zip
	zip -r INF2705_remise_$1.zip *.cpp *.h *.glsl makefile *.txt ${TEXTURES}
EOF
}

doremise()
{
cat <<EOF
zip -r INF2705_remise_$1.zip *.cpp *.h *.glsl makefile *.txt ${TEXTURES} && echo "remettre fichier INF2705_remise_$1.zip fait avec zip"
IF %ERRORLEVEL% EQU 0 GOTO Fin

7z a -r INF2705_remise_$1.7z *.cpp *.h *.glsl makefile *.txt ${TEXTURES} && echo "remettre fichier INF2705_remise_$1.7z fait avec 7z"
IF %ERRORLEVEL% EQU 0 GOTO Fin

rar a INF2705_remise_$1.rar *.cpp *.h *.glsl makefile *.txt ${TEXTURES} && echo "remettre fichier INF2705_remise_$1.tar fait avec rar"
IF %ERRORLEVEL% EQU 0 GOTO Fin

tar cvf INF2705_remise_$1.tar *.cpp *.h *.glsl makefile *.txt ${TEXTURES} && echo "remettre fichier INF2705_remise_$1.tar fait avec tar"
IF %ERRORLEVEL% EQU 0 GOTO Fin

echo "Ne sait pas comment faire INF2705_remise_$1..."

:Fin
EOF
}

# $compress = @{
# LiteralPath= Get-Childitem "./*.cpp", "./*.h", "./*.glsl","./*.txt", "./makefile"
# DestinationPath = "./test.Zip"
# }
# Compress-Archive @compress

# powershell.exe -executionpolicy remotesigned -File ./remise.ps1

docmakelists()
{
INC=`echo *.h`
SRC=`echo *.cpp`
cat <<EOF
cmake_minimum_required(VERSION 3.14)
project($2)

set(CMAKE_CXX_STANDARD 11)

include_directories(.)

set(SOURCE_FILES
  ${INC}
  ${SRC})

add_executable($2 \${SOURCE_FILES})

find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(SDL2 REQUIRED)
include_directories(\${OPENGL_INCLUDE_DIRS}  \${GLEW_INCLUDE_DIRS} \${SDL2_INCLUDE_DIRS})

target_link_libraries($2 \${OPENGL_LIBRARIES} \${GLEW_LIBRARIES} \${SDL2_LIBRARIES})
EOF
}

for DIR in *-*/s??; do
    case ${DIR} in
    01-PremiersPas*) continue; ;;
    esac
    ( cd ${DIR} && domakefile ${DIR/-*/} `dirname ${DIR/-/_}` > makefile; )
#    ( cd ${DIR} && domakefile > makefile && g++ -MM -MG *.cpp | sed 's! GL/[^ ]*!!g' >> makefile; )
    ( cd ${DIR} && docmakelists ${DIR/-*/} `dirname ${DIR/-/_}` > CMakeLists.txt; )
    # seulement pour les TP :
    case ${DIR} in
        tp*)
            ( cd ${DIR} && domakefileremise ${DIR/-*/} `dirname ${DIR/-/_}` >> makefile; )
            ( cd ${DIR} && doremise ${DIR/-*/} `dirname ${DIR/-/_}` > remise.bat; )
            ;;
    esac

done


# -Wvla
# https://gcc.gnu.org/onlinedocs/gcc/Variable-Length.html
# https://stackoverflow.com/questions/17645496/are-variable-length-arrays-an-extension-in-clang-too

# -DGLM_FORCE_MESSAGES=1 pour voir la configration de GLM
