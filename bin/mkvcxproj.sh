#!/bin/sh

dosln()
{
cat <<EOF

Microsoft Visual Studio Solution File, Format Version 12.00
# Visual Studio 2013
VisualStudioVersion = 12.0.30501.0
MinimumVisualStudioVersion = 10.0.40219.1
Project("{8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942}") = "${NOM}", "${NOM}.vcxproj", "{21C099B6-346C-4911-BDE2-DA5D7915C146}"
EndProject
Global
	GlobalSection(SolutionConfigurationPlatforms) = preSolution
		Debug|Win32 = Debug|Win32
		Debug|x64 = Debug|x64
		Release|Win32 = Release|Win32
		Release|x64 = Release|x64
	EndGlobalSection
	GlobalSection(ProjectConfigurationPlatforms) = postSolution
		{21C099B6-346C-4911-BDE2-DA5D7915C146}.Debug|Win32.ActiveCfg = Debug|Win32
		{21C099B6-346C-4911-BDE2-DA5D7915C146}.Debug|Win32.Build.0 = Debug|Win32
		{21C099B6-346C-4911-BDE2-DA5D7915C146}.Debug|x64.ActiveCfg = Debug|x64
		{21C099B6-346C-4911-BDE2-DA5D7915C146}.Debug|x64.Build.0 = Debug|x64
		{21C099B6-346C-4911-BDE2-DA5D7915C146}.Release|Win32.ActiveCfg = Release|Win32
		{21C099B6-346C-4911-BDE2-DA5D7915C146}.Release|Win32.Build.0 = Release|Win32
		{21C099B6-346C-4911-BDE2-DA5D7915C146}.Release|x64.ActiveCfg = Release|x64
		{21C099B6-346C-4911-BDE2-DA5D7915C146}.Release|x64.Build.0 = Release|x64
	EndGlobalSection
	GlobalSection(SolutionProperties) = preSolution
		HideSolutionNode = FALSE
	EndGlobalSection
EndGlobal
EOF
}


dovcxproj()
{
### le debut
cat <<EOF
<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build" ToolsVersion="15.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <ItemGroup Label="ProjectConfigurations">
    <ProjectConfiguration Include="Debug|Win32">
      <Configuration>Debug</Configuration>
      <Platform>Win32</Platform>
    </ProjectConfiguration>
    <ProjectConfiguration Include="Debug|x64">
      <Configuration>Debug</Configuration>
      <Platform>x64</Platform>
    </ProjectConfiguration>
    <ProjectConfiguration Include="Release|Win32">
      <Configuration>Release</Configuration>
      <Platform>Win32</Platform>
    </ProjectConfiguration>
    <ProjectConfiguration Include="Release|x64">
      <Configuration>Release</Configuration>
      <Platform>x64</Platform>
    </ProjectConfiguration>
  </ItemGroup>
  <PropertyGroup Label="Globals">
    <ProjectName>${NOM}</ProjectName>
    <ProjectGuid>{21C099B6-346C-4911-BDE2-DA5D7915C146}</ProjectGuid>
    <Keyword>Win32Proj</Keyword>
    <RootNamespace>.</RootNamespace>
  </PropertyGroup>
  <Import Project="\$(VCTargetsPath)\Microsoft.Cpp.Default.props" />
  <PropertyGroup Condition="'\$(Configuration)|\$(Platform)'=='Debug|Win32'" Label="Configuration">
    <ConfigurationType>Application</ConfigurationType>
    <UseDebugLibraries>true</UseDebugLibraries>
    <PlatformToolset>v142</PlatformToolset>
    <CharacterSet>Unicode</CharacterSet>
  </PropertyGroup>
  <PropertyGroup Condition="'\$(Configuration)|\$(Platform)'=='Debug|x64'" Label="Configuration">
    <ConfigurationType>Application</ConfigurationType>
    <UseDebugLibraries>true</UseDebugLibraries>
    <PlatformToolset>v142</PlatformToolset>
    <CharacterSet>Unicode</CharacterSet>
  </PropertyGroup>
  <PropertyGroup Condition="'\$(Configuration)|\$(Platform)'=='Release|Win32'" Label="Configuration">
    <ConfigurationType>Application</ConfigurationType>
    <UseDebugLibraries>false</UseDebugLibraries>
    <PlatformToolset>v142</PlatformToolset>
    <WholeProgramOptimization>true</WholeProgramOptimization>
    <CharacterSet>Unicode</CharacterSet>
  </PropertyGroup>
  <PropertyGroup Condition="'\$(Configuration)|\$(Platform)'=='Release|x64'" Label="Configuration">
    <ConfigurationType>Application</ConfigurationType>
    <UseDebugLibraries>false</UseDebugLibraries>
    <PlatformToolset>v142</PlatformToolset>
    <WholeProgramOptimization>true</WholeProgramOptimization>
    <CharacterSet>Unicode</CharacterSet>
  </PropertyGroup>
  <Import Project="\$(VCTargetsPath)\Microsoft.Cpp.props" />
  <ImportGroup Label="ExtensionSettings">
  </ImportGroup>
  <ImportGroup Label="PropertySheets" Condition="'\$(Configuration)|\$(Platform)'=='Debug|Win32'">
    <Import Project="\$(UserRootDir)\Microsoft.Cpp.\$(Platform).user.props" Condition="exists('\$(UserRootDir)\Microsoft.Cpp.\$(Platform).user.props')" Label="LocalAppDataPlatform" />
  </ImportGroup>
  <ImportGroup Label="PropertySheets" Condition="'\$(Configuration)|\$(Platform)'=='Debug|x64'">
    <Import Project="\$(UserRootDir)\Microsoft.Cpp.\$(Platform).user.props" Condition="exists('\$(UserRootDir)\Microsoft.Cpp.\$(Platform).user.props')" Label="LocalAppDataPlatform" />
  </ImportGroup>
  <ImportGroup Label="PropertySheets" Condition="'\$(Configuration)|\$(Platform)'=='Release|Win32'">
    <Import Project="\$(UserRootDir)\Microsoft.Cpp.\$(Platform).user.props" Condition="exists('\$(UserRootDir)\Microsoft.Cpp.\$(Platform).user.props')" Label="LocalAppDataPlatform" />
  </ImportGroup>
  <ImportGroup Label="PropertySheets" Condition="'\$(Configuration)|\$(Platform)'=='Release|x64'">
    <Import Project="\$(UserRootDir)\Microsoft.Cpp.\$(Platform).user.props" Condition="exists('\$(UserRootDir)\Microsoft.Cpp.\$(Platform).user.props')" Label="LocalAppDataPlatform" />
  </ImportGroup>
  <PropertyGroup Label="UserMacros" />
  <PropertyGroup Condition="'\$(Configuration)|\$(Platform)'=='Debug|Win32'">
    <LinkIncremental>true</LinkIncremental>
  </PropertyGroup>
  <PropertyGroup Condition="'\$(Configuration)|\$(Platform)'=='Debug|x64'">
    <LinkIncremental>true</LinkIncremental>
  </PropertyGroup>
  <PropertyGroup Condition="'\$(Configuration)|\$(Platform)'=='Release|Win32'">
    <LinkIncremental>false</LinkIncremental>
  </PropertyGroup>
  <PropertyGroup Condition="'\$(Configuration)|\$(Platform)'=='Release|x64'">
    <LinkIncremental>false</LinkIncremental>
  </PropertyGroup>
  <ItemDefinitionGroup Condition="'\$(Configuration)|\$(Platform)'=='Debug|Win32'">
    <ClCompile>
      <PrecompiledHeader>
      </PrecompiledHeader>
      <WarningLevel>Level3</WarningLevel>
      <Optimization>Disabled</Optimization>
      <PreprocessorDefinitions>WIN32;_CRT_SECURE_NO_WARNINGS;_DEBUG;_CONSOLE;_LIB;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <SDLCheck>true</SDLCheck>
      <LanguageStandard>stdcpp17</LanguageStandard>
    </ClCompile>
    <Link>
      <SubSystem>Console</SubSystem>
      <GenerateDebugInformation>true</GenerateDebugInformation>
      <AdditionalDependencies>opengl32.lib</AdditionalDependencies>
    </Link>
  </ItemDefinitionGroup>
  <ItemDefinitionGroup Condition="'\$(Configuration)|\$(Platform)'=='Debug|x64'">
    <ClCompile>
      <PrecompiledHeader>
      </PrecompiledHeader>
      <WarningLevel>Level3</WarningLevel>
      <Optimization>Disabled</Optimization>
      <PreprocessorDefinitions>WIN32;_CRT_SECURE_NO_WARNINGS;_DEBUG;_CONSOLE;_LIB;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <SDLCheck>true</SDLCheck>
      <LanguageStandard>stdcpp17</LanguageStandard>
    </ClCompile>
    <Link>
      <SubSystem>Console</SubSystem>
      <GenerateDebugInformation>true</GenerateDebugInformation>
      <AdditionalDependencies>opengl32.lib</AdditionalDependencies>
    </Link>
  </ItemDefinitionGroup>
  <ItemDefinitionGroup Condition="'\$(Configuration)|\$(Platform)'=='Release|Win32'">
    <ClCompile>
      <WarningLevel>Level3</WarningLevel>
      <PrecompiledHeader>
      </PrecompiledHeader>
      <Optimization>MaxSpeed</Optimization>
      <FunctionLevelLinking>true</FunctionLevelLinking>
      <IntrinsicFunctions>true</IntrinsicFunctions>
      <PreprocessorDefinitions>WIN32;_CRT_SECURE_NO_WARNINGS;NDEBUG;_CONSOLE;_LIB;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <SDLCheck>true</SDLCheck>
      <LanguageStandard>stdcpp17</LanguageStandard>
    </ClCompile>
    <Link>
      <SubSystem>Console</SubSystem>
      <GenerateDebugInformation>true</GenerateDebugInformation>
      <EnableCOMDATFolding>true</EnableCOMDATFolding>
      <OptimizeReferences>true</OptimizeReferences>
      <AdditionalDependencies>opengl32.lib</AdditionalDependencies>
    </Link>
  </ItemDefinitionGroup>
  <ItemDefinitionGroup Condition="'\$(Configuration)|\$(Platform)'=='Release|x64'">
    <ClCompile>
      <WarningLevel>Level3</WarningLevel>
      <PrecompiledHeader>
      </PrecompiledHeader>
      <Optimization>MaxSpeed</Optimization>
      <FunctionLevelLinking>true</FunctionLevelLinking>
      <IntrinsicFunctions>true</IntrinsicFunctions>
      <PreprocessorDefinitions>WIN32;_CRT_SECURE_NO_WARNINGS;NDEBUG;_CONSOLE;_LIB;%(PreprocessorDefinitions)</PreprocessorDefinitions>
      <SDLCheck>true</SDLCheck>
      <LanguageStandard>stdcpp17</LanguageStandard>
    </ClCompile>
    <Link>
      <SubSystem>Console</SubSystem>
      <GenerateDebugInformation>true</GenerateDebugInformation>
      <EnableCOMDATFolding>true</EnableCOMDATFolding>
      <OptimizeReferences>true</OptimizeReferences>
      <AdditionalDependencies>opengl32.lib</AdditionalDependencies>
    </Link>
  </ItemDefinitionGroup>
EOF

### les cpp
cat <<EOF
  <ItemGroup>
EOF
for FICHIER in `echo *.cpp`; do
case ${FICHIER} in
    *solution*|*Solution*) continue; ;;
esac
cat <<EOF
    <ClCompile Include="${FICHIER}" />
EOF
done
cat <<EOF
  </ItemGroup>
EOF

### les include
cat <<EOF
  <ItemGroup>
EOF
for FICHIER in `echo *.h`; do
case ${FICHIER} in
    *solution*|*Solution*) continue; ;;
esac
cat <<EOF
    <ClInclude Include="${FICHIER}" />
EOF
done
cat <<EOF
  </ItemGroup>
EOF

### les nuanceurs
if [ "`ls *.glsl 2>/dev/null`" ]; then
cat <<EOF
  <ItemGroup>
EOF
for FICHIER in `echo *.glsl`; do
case ${FICHIER} in
    *solution*|*Solution*) continue; ;;
esac
cat <<EOF
    <None Include="${FICHIER}" />
EOF
done
cat <<EOF
  </ItemGroup>
EOF
fi

### la fin
cat <<EOF
  <ItemGroup>
    <None Include="packages.config" />
  </ItemGroup>
  <Import Project="\$(VCTargetsPath)\Microsoft.Cpp.targets" />
  <ImportGroup Label="ExtensionTargets">
    <Import Project="packages\sdl2.${VER_SDL}\build\native\sdl2.targets" Condition="Exists('packages\sdl2.${VER_SDL}\build\native\sdl2.targets')" />
    <Import Project="packages\sdl2.redist.${VER_SDL_RED}\build\native\sdl2.redist.targets" Condition="Exists('packages\sdl2.redist.${VER_SDL_RED}\build\native\sdl2.redist.targets')" />
    <Import Project="packages\glm.${VER_GLM}\build\native\glm.targets" Condition="Exists('packages\glm.${VER_GLM}\build\native\glm.targets')" />
    <Import Project="packages\glew-2.2.0.${VER_GLEW}\build\native\glew-2.2.0.targets" Condition="Exists('packages\glew-2.2.0.${VER_GLEW}\build\native\glew-2.2.0.targets')" />
    <Import Project="packages\native.freeimage.3.17.0\build\native\freeimage.targets" Condition="Exists('packages\native.freeimage.3.17.0\build\native\freeimage.targets')" />
    <Import Project="packages\native.freeimage.redist.3.17.0\build\native\freeimage.redist.targets" Condition="Exists('packages\native.freeimage.redist.3.17.0\build\native\freeimage.redist.targets')" />
  </ImportGroup>
  <Target Name="EnsureNuGetPackageBuildImports" BeforeTargets="PrepareForBuild">
    <PropertyGroup>
      <ErrorText>Ce projet fait référence à des packages NuGet qui sont manquants sur cet ordinateur. Utilisez l'option de restauration des packages NuGet pour les télécharger. Pour plus d'informations, consultez http://go.microsoft.com/fwlink/?LinkID=322105. Le fichier manquant est : {0}.</ErrorText>
    </PropertyGroup>
    <Error Condition="!Exists('packages\sdl2.${VER_SDL}\build\native\sdl2.targets')" Text="\$([System.String]::Format('\$(ErrorText)', 'packages\sdl2.${VER_SDL}\build\native\sdl2.targets'))" />
    <Error Condition="!Exists('packages\sdl2.redist.${VER_SDL_RED}\build\native\sdl2.redist.targets')" Text="\$([System.String]::Format('\$(ErrorText)', 'packages\sdl2.redist.${VER_SDL_RED}\build\native\sdl2.redist.targets'))" />
    <Error Condition="!Exists('packages\glm.${VER_GLM}\build\native\glm.targets')" Text="\$([System.String]::Format('\$(ErrorText)', 'packages\glm.${VER_GLM}\build\native\glm.targets'))" />
    <Error Condition="!Exists('packages\glew-2.2.0.${VER_GLEW}\build\native\glew-2.2.0.targets')" Text="\$([System.String]::Format('\$(ErrorText)', 'packages\glew-2.2.0.${VER_GLEW}\build\native\glew-2.2.0.targets'))" />
    <Error Condition="!Exists('packages\native.freeimage.3.17.0\build\native\freeimage.targets')" Text="\$([System.String]::Format('\$(ErrorText)', 'packages\native.freeimage.3.17.0\build\native\freeimage.targets'))" />
    <Error Condition="!Exists('packages\native.freeimage.redist.3.17.0\build\native\freeimage.redist.targets')" Text="\$([System.String]::Format('\$(ErrorText)', 'packages\native.freeimage.redist.3.17.0\build\native\freeimage.redist.targets'))" />
  </Target>
</Project>
EOF
}

#<Import Project="packages\sdl2_ttf.nuget.2.0.15\build\native\sdl2_ttf.nuget.targets" Condition="Exists('packages\sdl2_ttf.nuget.2.0.15\build\native\sdl2_ttf.nuget.targets')" />
#<Error Condition="!Exists('packages\sdl2_ttf.nuget.2.0.15\build\native\sdl2_ttf.nuget.targets')" Text="$([System.String]::Format('$(ErrorText)', 'packages\sdl2_ttf.nuget.2.0.15\build\native\sdl2_ttf.nuget.targets'))" />

dovcxprojfilters()
{
### le debut
cat <<EOF
<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <ItemGroup>
    <Filter Include="Fichiers sources">
      <UniqueIdentifier>{4FC737F1-C7A5-4376-A066-2A32D752A2FF}</UniqueIdentifier>
      <Extensions>cpp;c;cc;cxx;def;odl;idl;hpj;bat;asm;asmx</Extensions>
    </Filter>
    <Filter Include="Fichiers d%27en-tête">
      <UniqueIdentifier>{93995380-89BD-4b04-88EB-625FBE52EBFB}</UniqueIdentifier>
      <Extensions>h;hh;hpp;hxx;hm;inl;inc;xsd</Extensions>
    </Filter>
    <Filter Include="Fichiers de ressources">
      <UniqueIdentifier>{67DA6AB6-F800-4c08-8B7A-83BB121AAD01}</UniqueIdentifier>
      <Extensions>rc;ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe;resx;tiff;tif;png;wav;mfcribbon-ms</Extensions>
    </Filter>
    <Filter Include="Nuanceurs">
      <UniqueIdentifier>{b86257d9-ccba-4a79-b007-44972d342efc}</UniqueIdentifier>
    </Filter>
  </ItemGroup>
EOF

### les cpp
cat <<EOF
  <ItemGroup>
EOF
for FICHIER in `echo *.cpp`; do
case ${FICHIER} in
    *solution*|*Solution*) continue; ;;
esac
cat <<EOF
    <ClCompile Include="${FICHIER}">
      <Filter>Fichiers sources</Filter>
    </ClCompile>
EOF
done
cat <<EOF
  </ItemGroup>
EOF

### les include
cat <<EOF
  <ItemGroup>
EOF
for FICHIER in `echo *.h | grep -v solution`; do
case ${FICHIER} in
    *solution*|*Solution*) continue; ;;
esac
cat <<EOF
    <ClInclude Include="${FICHIER}">
      <Filter>Fichiers d%27en-tête</Filter>
    </ClInclude>
EOF
done
cat <<EOF
  </ItemGroup>
  <ItemGroup>
    <None Include="packages.config" />
  </ItemGroup>
EOF

### la fin
cat <<EOF
</Project>
EOF
}

dovcxprojuser()
{
cat <<EOF
<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="12.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <PropertyGroup />
</Project>
EOF
}

dopackagesconfig()
{
  cat <<EOF
<?xml version="1.0" encoding="utf-8"?>
<packages>
  <package id="sdl2" version="${VER_SDL}" targetFramework="native" />
  <package id="sdl2.redist" version="${VER_SDL_RED}" targetFramework="native" />
  <package id="glm" version="${VER_GLM}" targetFramework="native" />
  <package id="glew-2.2.0" version="${VER_GLEW}" targetFramework="native" />
  <package id="native.freeimage" version="3.17.0" />
  <package id="native.freeimage.redist" version="3.17.0" />
</packages>
EOF
#<package id="sdl2_ttf.nuget" version="2.0.15" targetFramework="native" />
}

getlatestnugetver()
{
  PACKAGE="$1"
  >&2 echo "Getting latest version number for ${PACKAGE}..."
  ver=$(curl --progress-bar https://api.nuget.org/v3/registration3/${PACKAGE}/index.json) || \
    (>&2 echo "Failed to get version number for ${PACKAGE}, aborting..."; exit 1)
  ver=$(echo "$ver" | grep -oE '"?upper"?\s*:\s*"?[0-9.]*"?' | sed 's/"//g' | awk -F: '{print $2}')
  printf '%s' "$ver"
}

VER_GLM=$(getlatestnugetver "glm")
VER_GLM=0.9.9.800 # car la ligne précédente retourne incorrectement 0.9.9.700
VER_GLEW=$(getlatestnugetver "glew-2.2.0")
VER_GLEW=2.2.0.1
VER_SDL=$(getlatestnugetver "sdl2")
VER_SDL_RED=$(getlatestnugetver "sdl2.redist")

for DIR in *-*; do
    case ${DIR} in
    tp*webgl*) continue; ;;
    01-PremiersPas*) continue; ;;
    *zip) continue; ;;
    esac
    [ -f ${DIR}/src/main.cpp ] || continue
    NOM=00_${DIR/-/_};
    if [ -d ${DIR}/src ]; then
        (
            cd ${DIR}/src;
            dosln            > 00_${DIR/-/_}.sln;
            dovcxproj        > 00_${DIR/-/_}.vcxproj;
            dopackagesconfig > packages.config;
            # dovcxprojfilters > ${DIR/-/_}.vcxproj.filters;
            # dovcxprojuser    > ${DIR/-/_}.vcxproj.user;
        )
    fi
    if [ -d ${DIR}/sol ]; then
        (
            cd ${DIR}/sol;
            dosln            > 00_${DIR/-/_}.sln;
            dovcxproj        > 00_${DIR/-/_}.vcxproj;
            dopackagesconfig > packages.config;
            # dovcxprojfilters > ${DIR/-/_}.vcxproj.filters;
            # dovcxprojuser    > ${DIR/-/_}.vcxproj.user;
        )
    fi
done
