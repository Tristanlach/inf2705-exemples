# 04-StencilTheiere
<a href="images/image.png"><img width=200px src="images/image.png"></a>

Comme exercice, modifier l'affichage afin de produire l'image ci-dessous.<br>
(Débuter avec un test de stencil qui ne passe jamais (GL_NEVER) puis tracer une théière de plus.)<br>
<a href="images/imageExercice.png"><img width=200px src="images/imageExercice.png"></a>

