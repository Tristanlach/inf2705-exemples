Touches possibles :
    q:  Quitter l'application
    v:  Recharger les nuanceurs
    PLUS:  Afficher plus de contenu
    MOINS:  Afficher moins de contenu
    DROITE:  Déplacer le plan vers la droite
    GAUCHE:  Déplacer le plan vers la gauche
    s:  Sauvegarder une copie de la fenêtre dans un fichier
