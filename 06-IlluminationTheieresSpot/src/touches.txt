Touches possibles :
    q:  Quitter l'application
    x:  Activer/désactiver l'affichage des axes
    v:  Recharger les fichiers des nuanceurs et recréer le programme
    g:  Permuter l'affichage en fil de fer ou plein
    s:  Sauvegarder une copie de la fenêtre dans un fichier
    Molette:  Changer la taille du spot
