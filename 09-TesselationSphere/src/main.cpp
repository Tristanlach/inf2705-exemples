#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "Etat.h"
#include "Pipeline.h"
#include "Camera.h"

void chargerNuanceurs()
{
    // créer le programme
    prog = glCreateProgram();

    // attacher le nuanceur de sommets
    const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesSommets[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
        glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesSommets[1];
    }
    // attacher le nuanceur de controle de la tessellation
    const GLchar *chainesTessctrl[2] = { "#version 410\n#define NUANCEUR_TESSCTRL\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesTessctrl[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_TESS_CONTROL_SHADER );
        glShaderSource( nuanceurObj, 2, chainesTessctrl, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesTessctrl[1];
    }
    // attacher le nuanceur d'évaluation de la tessellation
    const GLchar *chainesTesseval[2] = { "#version 410\n#define NUANCEUR_TESSEVAL\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesTesseval[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_TESS_EVALUATION_SHADER );
        glShaderSource( nuanceurObj, 2, chainesTesseval, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesTesseval[1];
    }
    // attacher le nuanceur de géometrie
    const GLchar *chainesGeometrie[2] = { "#version 410\n#define NUANCEUR_GEOMETRIE\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesGeometrie[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_GEOMETRY_SHADER );
        glShaderSource( nuanceurObj, 2, chainesGeometrie, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesGeometrie[1];
    }
    // attacher le nuanceur de fragments
    const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesFragments[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
        glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesFragments[1];
    }
    // faire l'édition des liens du programme
    glLinkProgram( prog );
    ProgNuanceur::afficherLogLink( prog );

    // demander la "Location" des variables
    if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
    if ( ( locColor = glGetAttribLocation( prog, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
    if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
    if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
    if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
    if ( ( locTessLevelInner = glGetUniformLocation( prog, "TessLevelInner" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TessLevelInner" << std::endl;
    if ( ( locTessLevelOuter = glGetUniformLocation( prog, "TessLevelOuter" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TessLevelOuter" << std::endl;
    if ( ( locdeformerSphere = glGetUniformLocation( prog, "deformerSphere" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de deformerSphere" << std::endl;
}

static GLint Nconnec = 3*5*4; // 60

void FenetreTP::initialiser()
{
    // couleur de l'arrière-plan
    glm::vec4 couleurFond( 0.1, 0.1, 0.1, 1.0 );
    glClearColor( couleurFond.r, couleurFond.g, couleurFond.b, couleurFond.a );

    glEnable( GL_DEPTH_TEST );
    glDepthFunc( GL_LEQUAL );
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glEnable( GL_LINE_SMOOTH );
    glLineWidth( 2.0 );

    // charger les nuanceurs
    chargerNuanceurs();

    FenetreTP::VerifierErreurGL("debut initialiser");

    // Charger le modèle
#define X .525731112119133606
#define Z .850650808352039932

    static GLfloat icosaedreCoo[12][3] =
    {
        {-X,  0,  Z},
        { X,  0,  Z},
        {-X,  0, -Z},
        { X,  0, -Z},
        { 0,  Z,  X},
        { 0,  Z, -X},
        { 0, -Z,  X},
        { 0, -Z, -X},
        { Z,  X,  0},
        {-Z,  X,  0},
        { Z, -X,  0},
        {-Z, -X,  0}
    };

    static int icosaedreConnec[20][3] =
    {
        {4, 0, 1},
        {9, 0, 4},
        {5, 9, 4},
        {5, 4, 8},
        {8, 4, 1},
        {10, 8, 1},
        {3, 8, 10},
        {3, 5, 8},
        {2, 5, 3},
        {7, 2, 3},
        {10, 7, 3},
        {6, 7, 10},
        {11, 7, 6},
        {0, 11, 6},
        {1, 0, 6},
        {1, 6, 10},
        {0, 9, 11},
        {11, 9, 2},
        {2, 9, 5},
        {2, 7, 11},
    };

    const float coul[] =
    {
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,

        0.0, 1.0, 1.0,
        1.0, 0.0, 1.0,
        1.0, 1.0, 0.0,

        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0,

        0.0, 1.0, 1.0,
        1.0, 0.0, 1.0,
        1.0, 1.0, 0.0,
    };

    // allouer les objets OpenGL
    glGenVertexArrays( 1, vao );
    glBindVertexArray( vao[0] );

    glGenBuffers( 3, vbo );

    // charger le VBO pour les sommets
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(icosaedreCoo), icosaedreCoo, GL_STATIC_DRAW );
    glVertexAttribPointer( locVertex, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locVertex);
    // charger le VBO pour les couleurs
    glBindBuffer( GL_ARRAY_BUFFER, vbo[1] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(coul), coul, GL_STATIC_DRAW );
    glVertexAttribPointer( locColor, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locColor);
    // charger le VBO pour la connectivité
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo[2] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(icosaedreConnec), icosaedreConnec, GL_STATIC_DRAW );

    glBindVertexArray( 0 );
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    FenetreTP::VerifierErreurGL("fin initialiser");
}

void FenetreTP::conclure()
{
    glUseProgram( 0 );
    glDeleteVertexArrays( 1, vao );
    glDeleteBuffers( 3, vbo );
}

void afficherModele()
{
    // informer OpenGL du niveau de tessellation
    GLfloat TessLevelOuterTab[] = { Etat::TessLevelOuter, Etat::TessLevelOuter, Etat::TessLevelOuter, Etat::TessLevelOuter };
    GLfloat TessLevelInnerTab[] = { Etat::TessLevelInner, Etat::TessLevelInner, Etat::TessLevelInner, Etat::TessLevelInner };
    glPatchParameterfv( GL_PATCH_DEFAULT_OUTER_LEVEL, TessLevelOuterTab );
    glPatchParameterfv( GL_PATCH_DEFAULT_INNER_LEVEL, TessLevelInnerTab );

    // afin de mieux voir à l'écran, changer la largeur de ligne
    //{ int l = Etat::TessLevelInner+Etat::TessLevelOuter; glLineWidth( l < 3 ? 5. : l < 11 ? 4. : l < 17 ? 3. : l < 25 ? 2. : 1. ); }

    // afficher le triangle avec son programme qui gère les triangles
    glUseProgram( prog );
    glUniform1f( locTessLevelInner, Etat::TessLevelInner );
    glUniform1f( locTessLevelOuter, Etat::TessLevelOuter );
    glUniform1i( locdeformerSphere, (int) Etat::deformerSphere );
    glPatchParameteri( GL_PATCH_VERTICES, 3 );

    // initialiser les transformations de modélisation
    matrModel.LoadIdentity();
    matrModel.Rotate( Etat::angle, 1.0, 1.0, 1.0 );
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );

    // afficher le modèle
    glBindVertexArray( vao[0] );         // sélectionner le premier VAO
    glDrawElements( GL_PATCHES, Nconnec, GL_UNSIGNED_INT, 0 );
    glBindVertexArray( 0 );              // désélectionner le VAO

    // tourner la scêne
    if ( Etat::enmouvement )
    {
        Etat::angle += 1.1;
    }
}

void FenetreTP::afficherScene()
{
    // effacer l'ecran et le tampon de profondeur
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glUseProgram( prog );

    // mettre à jour la projection
    GLdouble aspect = (GLdouble) largeur_ / (GLdouble) hauteur_;
#if 1
    matrProj.Perspective( 15.0, aspect, 1.0, 20.0 );
#else
    matrProj.Ortho( -1.2*aspect, 1.2*aspect, -1.2, 1.2, 1.0, 20.0 );
#endif
    glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

    // mettre à jour la caméra
    matrVisu.LookAt( camera.dist*cos(glm::radians(camera.phi))*sin(glm::radians(camera.theta)),
                     camera.dist*sin(glm::radians(camera.phi))*sin(glm::radians(camera.theta)),
                     camera.dist*cos(glm::radians(camera.theta)),
                     0.0, 0.0, 0.0,
                     0.0, 1.0, 0.0 );
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    // dessiner la scène
    afficherModele();

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_g: // Permuter l'affichage en fil de fer ou plein
        {
            GLint modePlein[2];
            glGetIntegerv( GL_POLYGON_MODE, modePlein );
            glPolygonMode( GL_FRONT_AND_BACK, ( modePlein[0] == GL_LINE ) ? GL_FILL : GL_LINE );
        }
        break;

    case TP_ESPACE: // Mettre en pause ou reprendre l'animation
        Etat::enmouvement = !Etat::enmouvement;
        break;

    case TP_SOULIGNE:
    case TP_MOINS: // Incrémenter la distance de la caméra
        camera.dist += 0.5;
        break;

    case TP_PLUS: // Décrémenter la distance de la caméra
    case TP_EGAL:
        camera.dist -= 0.5;
        break;

    case TP_0: // Remettre les angles de la caméra à 0
        camera.theta = camera.phi = 0;
        Etat::angle = 0;
        break;

    case TP_GAUCHE: // Augmenter theta
        camera.theta += 0.5;
        break;

    case TP_DROITE: // Décrémenter theta
        camera.theta -= 0.5;
        break;

    case TP_HAUT: // Augmenter phi
        camera.phi += 0.5;
        break;

    case TP_BAS: // Décrémenter phi
        camera.phi -= 0.5;
        break;

    case TP_d: // Déformer la sphère
        Etat::deformerSphere = !Etat::deformerSphere;
        std::cout << " Etat::deformerSphere=" << Etat::deformerSphere << std::endl;
        break;

    case TP_i: // Augmenter le niveau de tessellation interne
        ++Etat::TessLevelInner;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;
    case TP_k: // Diminuer le niveau de tessellation interne
        if ( --Etat::TessLevelInner < 1 ) Etat::TessLevelInner = 1;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;

    case TP_o: // Augmenter le niveau de tessellation externe
        ++Etat::TessLevelOuter;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;
    case TP_l: // Diminuer le niveau de tessellation externe
        if ( --Etat::TessLevelOuter < 1 ) Etat::TessLevelOuter = 1;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;

    case TP_u: // Augmenter les deux niveaux de tessellation
        ++Etat::TessLevelOuter;
        Etat::TessLevelInner = Etat::TessLevelOuter;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;
    case TP_j: // Diminuer les deux niveaux de tessellation
        if ( --Etat::TessLevelOuter < 1 ) Etat::TessLevelOuter = 1;
        Etat::TessLevelInner = Etat::TessLevelOuter;
        std::cout << " Etat::TessLevelInner=" << Etat::TessLevelInner << " Etat::TessLevelOuter=" << Etat::TessLevelOuter << std::endl;
        break;

    case TP_s: // Sauvegarder une copie de la fenêtre dans un fichier
        sauvegarderFenetre( );
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        break;
    }
}

void FenetreTP::sourisClic( int button, int state, int x, int y )
{
}

void FenetreTP::sourisMolette( int x, int y )
{
}

void FenetreTP::sourisMouvement( int x, int y )
{
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "tess" );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
