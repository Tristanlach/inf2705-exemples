Touches possibles :
    q:  Quitter l'application
    v:  Recharger les nuanceurs
    g:  Permuter l'affichage en fil de fer ou plein
    ESPACE:  Mettre en pause ou reprendre l'animation
    MOINS:  Incrémenter la distance de la caméra
    PLUS:  Décrémenter la distance de la caméra
    0:  Remettre les angles de la caméra à 0
    GAUCHE:  Augmenter theta
    DROITE:  Décrémenter theta
    HAUT:  Augmenter phi
    BAS:  Décrémenter phi
    d:  Déformer la sphère
    i:  Augmenter le niveau de tessellation interne
    k:  Diminuer le niveau de tessellation interne
    o:  Augmenter le niveau de tessellation externe
    l:  Diminuer le niveau de tessellation externe
    u:  Augmenter les deux niveaux de tessellation
    j:  Diminuer les deux niveaux de tessellation
    s:  Sauvegarder une copie de la fenêtre dans un fichier
