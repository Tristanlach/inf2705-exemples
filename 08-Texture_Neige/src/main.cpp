#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "inf2705-texture.h"

GLuint prog;
GLint locVertex, locTexCoord, locsolution, loctemps;
GLint locmatrModel, locmatrVisu, locmatrProj;
GLint loclaTextureScene, loclaTextureNeige;
GLuint vao[1], vbo[3];

// les matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

// la solution
int solution = 2;
// le temps courant dans la simulation (en secondes)
float temps = 0.0;
bool enmouvement = true;  // le modèle est en mouvement/rotation automatique ou non

// la fonction habituelle!
void chargerNuanceurs()
{
    // créer le programme
    prog = glCreateProgram();
    // attacher le nuanceur de sommets
    const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesSommets[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
        glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesSommets[1];
    }
    // attacher le nuanceur de fragments
    const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesFragments[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
        glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesFragments[1];
    }
    // faire l'édition des liens du programme
    glLinkProgram( prog );
    ProgNuanceur::afficherLogLink( prog );

    if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
    if ( ( locTexCoord = glGetAttribLocation( prog, "TexCoord" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TexCoord" << std::endl;
    if ( ( locsolution = glGetUniformLocation( prog, "solution" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de solution" << std::endl;
    if ( ( loctemps = glGetUniformLocation( prog, "temps" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de temps" << std::endl;
    if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
    if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
    if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
    if ( ( loclaTextureScene = glGetUniformLocation( prog, "laTextureScene" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTextureScene" << std::endl;
    if ( ( loclaTextureNeige = glGetUniformLocation( prog, "laTextureNeige" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTextureNeige" << std::endl;
}

// avancer le temps
void calculerPhysique( )
{
    const float dt = GLfloat(1./60.);  // la carte graphique affiche 60 images/seconde
    if ( enmouvement ) temps += dt;
}

// la fonction habituelle pour mettre les images dans les textures en mémoire!
bool chargerTexture( std::string fichier, GLuint &texture )
{
    GLsizei largeur, hauteur;
    unsigned char *pixels;
    if ( ( pixels = ChargerImage( fichier, largeur, hauteur ) ) != NULL )
    {
        glGenTextures( 1, &texture );
        glBindTexture( GL_TEXTURE_2D, texture );
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB,
                      largeur, hauteur, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
        glBindTexture( GL_TEXTURE_2D, 0 );
        delete[] pixels;
    }
    return true;
}

void initialiser()
{
    // charger les deux textures
    GLuint maTextureScene;
    GLuint maTextureNeige;
    chargerTexture( std::string("textures/scene.bmp"), maTextureScene );
    chargerTexture( std::string("textures/neige.bmp"), maTextureNeige );

    // assigner chaque image dans une unité de texture différente
    glActiveTexture( GL_TEXTURE0 ); // l'unité de texture 0
    glBindTexture( GL_TEXTURE_2D, maTextureScene );
    glActiveTexture( GL_TEXTURE1 ); // l'unité de texture 1
    glBindTexture( GL_TEXTURE_2D, maTextureNeige );

    // charger les nuanceurs (tous vos fichiers de nuanceurs seront chargés ici)
    chargerNuanceurs();

    // Le modèle
    const GLfloat coords[] = { -1.0, -1.0,   1.0, -1.0,   1.0,  1.0,   -1.0,  1.0 };
    const GLfloat texcoo[] = {  0.0,  0.0,   1.0,  0.0,   1.0,  1.0,    0.0,  1.0 };
    const GLuint connec[] = {  0, 1, 2,   2, 3, 0  };

    // allouer les objets OpenGL
    glGenVertexArrays( 1, vao );
    glGenBuffers( 3, vbo );

    // initialiser le VAO
    glBindVertexArray( vao[0] );

    // charger le VBO pour les sommets
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(coords), coords, GL_STATIC_DRAW );
    glVertexAttribPointer( locVertex, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locVertex);

    // charger le VBO pour la connectivité
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo[1] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(connec), connec, GL_STATIC_DRAW );

    // charger le VBO pour les coordonnées de texture
    glBindBuffer( GL_ARRAY_BUFFER, vbo[2] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(texcoo), texcoo, GL_STATIC_DRAW );
    glVertexAttribPointer( locTexCoord, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locTexCoord);

    glBindVertexArray( 0 );
}

// afficher la scène à chaque pas de temps
void FenetreTP::afficherScene()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // assigner les variables uniformes
    glUseProgram( prog );
    glUniform1i( loclaTextureScene, 0 ); // unité de texture 0
    glUniform1i( loclaTextureNeige, 1 ); // unité de texture 1
    glUniform1i( locsolution, solution ); // la solution courante
    glUniform1f( loctemps, temps ); // le temps courant

    // transformation de projection orthogonale
    matrProj.Ortho( -1.0, 1.0, -1.0, 1.0, -1.0, 1.0 );
    glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

    // transformation de visualisation
    matrVisu.LoadIdentity( );
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    // transformation de modélisation
    matrModel.LoadIdentity( );
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );

    // afficher les deux triangles qui forment le carré (6 indices connec[])
    glBindVertexArray( vao[0] );
    glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );
    glBindVertexArray( 0 );
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q:
        quit();
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_0: // Remettre à zéro
        temps = 0.;
        break;

    case TP_1: // Solution 1
        temps = 0.;
        solution = 1;
        std::cout << " solution=" << solution << std::endl;
        break;

    case TP_2: // Solution 2
        temps = 0.;
        solution = 2;
        std::cout << " solution=" << solution << std::endl;
        break;

    case TP_3: // Solution 3
        temps = 0.;
        solution = 3;
        std::cout << " solution=" << solution << std::endl;
        break;

    case TP_PLUS:
    case TP_EGAL:
        break;

    case TP_SOULIGNE:
    case TP_MOINS:
        break;

    case TP_g:
        {
            static GLint modePlein;
            glGetIntegerv( GL_POLYGON_MODE, &modePlein );
            if ( modePlein == GL_LINE )
                glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
            else
                glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        }
        break;

    case TP_ESPACE: // Mettre en pause ou reprendre l'animation
        enmouvement = !enmouvement;
        break;

    case TP_s: // Sauvegarder une copie de la fenêtre dans un fichier
        sauvegarderFenetre( );
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        break;
    }
}

void FenetreTP::sourisClic( int button, int state, int x, int y )
{
}

void FenetreTP::sourisMolette( int x, int y )
{
}

void FenetreTP::sourisMouvement( int x, int y )
{
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "texture", 500, 500 );
    //FenetreTP fenetre( "texture", 1900, 1900 );

    // allouer des ressources et définir le contexte OpenGL
    initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // mettre à jour la physique
        calculerPhysique( );

        // affichage
        fenetre.afficherScene();
        fenetre.swap();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    return 0;
}
