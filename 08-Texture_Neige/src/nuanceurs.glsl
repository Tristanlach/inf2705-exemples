// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

uniform mat4 matrModel, matrVisu, matrProj;

in vec4 Vertex;
in vec2 TexCoord;

out Attribs { vec2 texCoord; } AttribsOut;

void main( void )
{
    // transformation standard du sommet
    gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // transmettre au nuanceur de fragments les coordonnées de texture reçues
    AttribsOut.texCoord = TexCoord;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

uniform int solution;
uniform float temps;
uniform sampler2D laTextureScene, laTextureNeige;

in Attribs { vec2 texCoord; } AttribsIn;

out vec4 FragColor;

void main( void )
{
    switch ( solution )
    {
    default:
    case 1:
        {
            vec4 coulScene = texture( laTextureScene, AttribsIn.texCoord );
            vec4 coulNeige = texture( laTextureNeige, AttribsIn.texCoord );
            // on peut simplement ajouter les deux couleurs l'une à l'autre :
            // le noir ne changera pas la couleur de la scène et le blanc la saturera
            FragColor = clamp( coulScene + coulNeige, 0.0, 1.0 );

            // FragColor = ( coulNeige.rgb == vec3(0.0) ) ? coulScene : coulNeige; //  NON car ne tient pas compte de l'aliassage qui donne du gris
        }
        break;
    case 2:
        {
            vec4 coulScene = texture( laTextureScene, AttribsIn.texCoord );
            // on échantillone la neige en utilisant le temps : à chaque seconde, la texture se répète
            vec4 coulNeige = texture( laTextureNeige, AttribsIn.texCoord + vec2(0.0,temps) );

            //coulNeige = texture( laTextureNeige, AttribsIn.texCoord * vec2(1.0,temps) ); //  NON car plus on avance dans le temps, plus la neige est écrasée en y
            //coulNeige = texture( laTextureNeige, AttribsIn.texCoord + vec2(0.0,temps%1 ); // l'opérateur % doit être avec des entiers
            FragColor = clamp( coulScene + coulNeige, 0.0, 1.0 );
        }
        break;
    case 3:
        {
            vec4 coulScene = texture( laTextureScene, AttribsIn.texCoord );
            // on doit prendre 4 échantillons de la neige et les superposer
            vec4 coulNeige = texture( laTextureNeige, AttribsIn.texCoord + vec2(0.0,temps) );
            coulNeige     += texture( laTextureNeige, AttribsIn.texCoord + vec2(0.5,temps) );
            coulNeige     += texture( laTextureNeige, AttribsIn.texCoord + vec2(0.5,temps+0.5) );
            coulNeige     += texture( laTextureNeige, AttribsIn.texCoord + vec2(0.0,temps+0.5) );

            //coulNeige     = texture( laTextureNeige, 2.*AttribsIn.texCoord + vec2(0.0,temps+0.5) ); // NON car ça change la taille et ça rapetisse les flocons
            FragColor = clamp( coulScene + coulNeige, 0.0, 1.0 );
        }
        break;
    }
}

#endif
