#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint prog;
GLint locVertex, locTexCoord, locposcour, locsolution;
GLint locmatrModel, locmatrVisu, locmatrProj;
GLint loclaTextureTulipes, loclaTextureEchiquier, loclaTextureBallon, loclaTextureScene;

// matrices de du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
