#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "inf2705-texture.h"
#include "Pipeline.h"

int solution = 0;

// la fonction habituelle!
void chargerNuanceurs()
{
    // créer le programme
    prog = glCreateProgram();
    // attacher le nuanceur de sommets
    const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesSommets[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
        glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesSommets[1];
    }
    // attacher le nuanceur de fragments
    const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesFragments[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
        glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesFragments[1];
    }
    // faire l'édition des liens du programme
    glLinkProgram( prog );
    ProgNuanceur::afficherLogLink( prog );

    // demander la "Location" des variables
    if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
    if ( ( locTexCoord = glGetAttribLocation( prog, "TexCoord" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TexCoord" << std::endl;
    if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
    if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
    if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
    if ( ( loclaTextureTulipes = glGetUniformLocation( prog, "laTextureTulipes" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTextureTulipes" << std::endl;
    if ( ( loclaTextureEchiquier = glGetUniformLocation( prog, "laTextureEchiquier" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTextureEchiquier" << std::endl;
    if ( ( loclaTextureBallon = glGetUniformLocation( prog, "laTextureBallon" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTextureBallon" << std::endl;
    if ( ( loclaTextureScene = glGetUniformLocation( prog, "laTextureScene" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTextureScene" << std::endl;
    if ( ( locsolution = glGetUniformLocation( prog, "solution" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de solution" << std::endl;

    FenetreTP::VerifierErreurGL("fin chargerNuanceurs");
}
bool chargerTexture( std::string fichier, GLuint &texture )
{
    GLsizei largeur, hauteur;
    unsigned char *pixels;
    if ( ( pixels = ChargerImage( fichier, largeur, hauteur ) ) != NULL )
    {
        glGenTextures( 1, &texture );
        glBindTexture( GL_TEXTURE_2D, texture );
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB,
                      largeur, hauteur, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
        glBindTexture( GL_TEXTURE_2D, 0 );
        delete[] pixels;
    }
    return true;
}

void FenetreTP::initialiser()
{
    // charger les trois textures et retourner son identificateur dans maTexture...
    GLuint maTextureTulipes=0, maTextureEchiquier=0, maTextureBallon=0, maTextureScene=0;
    chargerTexture( std::string("textures/tulipes.bmp"), maTextureTulipes );
    chargerTexture( std::string("textures/echiquier.bmp"), maTextureEchiquier );
    chargerTexture( std::string("textures/ballon.bmp"), maTextureBallon );
    chargerTexture( std::string("textures/scene.bmp"), maTextureScene );

    // assigner chaque image dans une unité de texture différente
    glActiveTexture( GL_TEXTURE0 ); // l'unité de texture 0
    glBindTexture( GL_TEXTURE_2D, maTextureTulipes );
    glActiveTexture( GL_TEXTURE1 ); // l'unité de texture 1
    glBindTexture( GL_TEXTURE_2D, maTextureEchiquier );
    glActiveTexture( GL_TEXTURE2 ); // l'unité de texture 2
    glBindTexture( GL_TEXTURE_2D, maTextureBallon );
    glActiveTexture( GL_TEXTURE3 ); // l'unité de texture 3
    glBindTexture( GL_TEXTURE_2D, maTextureScene );

    // charger les nuanceurs (tous vos fichiers de nuanceurs seront chargés ici)
    chargerNuanceurs();

    // Le modèle
    const GLfloat coords[] = { -1.0, -1.0,   1.0, -1.0,   1.0,  1.0,   -1.0,  1.0 };
    const GLfloat texcoo[] = {  0.0,  0.0,   1.0,  0.0,   1.0,  1.0,    0.0,  1.0 };
    const GLuint connec[] = {  0, 1, 2,   2, 3, 0  };

    // allouer les objets OpenGL
    GLuint vao[1];  glGenVertexArrays( 1, vao );
    GLuint vbo[3];  glGenBuffers( 3, vbo );

    // initialiser le VAO
    glBindVertexArray( vao[0] );

    // charger le VBO pour les sommets
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(coords), coords, GL_STATIC_DRAW );
    glVertexAttribPointer( locVertex, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locVertex);

    // charger le VBO pour la connectivité
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo[1] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(connec), connec, GL_STATIC_DRAW );

    // charger le VBO pour les coordonnées de texture
    glBindBuffer( GL_ARRAY_BUFFER, vbo[2] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(texcoo), texcoo, GL_STATIC_DRAW );
    glVertexAttribPointer( locTexCoord, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locTexCoord);
}

void FenetreTP::afficherScene()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // assigner les variables uniformes
    glUseProgram( prog );
    glUniform1i( loclaTextureTulipes, 0 ); // unité de texture 0
    glUniform1i( loclaTextureEchiquier, 1 ); // unité de texture 1
    glUniform1i( loclaTextureBallon, 2 ); // unité de texture 2
    glUniform1i( loclaTextureScene, 3 ); // unité de texture 3
    glUniform1i( locsolution, solution );

    // transformation de projection orthogonale
    matrProj.Ortho( -1.0, 1.0, -1.0, 1.0, -1.0, 1.0 );
    glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

    // transformation de visualisation
    matrVisu.LookAt( 0.0, 0.0, 0.0,  0.0, 0.0, -1.0,  0.0, 1.0, 0.0 );
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    // transformation de modélisation
    matrModel.LoadIdentity();
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );

    // afficher les deux triangles qui forment le carré (6 indices dans connec[])
    glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q:
        quit();
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_g:
        {
            static GLint modePlein;
            glGetIntegerv( GL_POLYGON_MODE, &modePlein );
            if ( modePlein == GL_LINE )
                glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
            else
                glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        }
        break;

    case TP_0: solution = 0; break; // image de base
    case TP_1: solution = 1; break; // solution 1
    case TP_2: solution = 2; break; // solution 2
    case TP_3: solution = 3; break; // solution 3
    case TP_4: solution = 4; break; // solution 4
    case TP_5: solution = 5; break; // solution 5
    case TP_6: solution = 6; break; // solution 6
    case TP_7: solution = 7; break; // solution 7
    case TP_8: solution = 8; break; // solution 8
    case TP_9: solution = 9; break; // solution 9

    case TP_s: // Sauvegarder une copie de la fenêtre dans un fichier
        sauvegarderFenetre( );
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        break;
    }
}

static bool presse = false;
void FenetreTP::sourisClic( int button, int state, int x, int y )
{ presse = ( state == TP_PRESSE ); }

void FenetreTP::sourisMolette( int x, int y )
{ }

void FenetreTP::sourisMouvement( int x, int y )
{ }

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "texture", 500, 500 );
    //FenetreTP fenetre( "texture", 1000, 1000 ); // pour les images dans l'examen ? mais tulipes est à 500x500

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }
    return 0;
}
