cmake_minimum_required(VERSION 3.14)
project(04_StencilMiroir)

set(CMAKE_CXX_STANDARD 11)

include_directories(.)

set(SOURCE_FILES
  Etat.h Pipeline.h inf2705-Singleton.h inf2705-fenetre.h inf2705-forme.h inf2705-matrice.h inf2705-nuanceur.h
  Etat.cpp main.cpp)

add_executable(04_StencilMiroir ${SOURCE_FILES})

find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(SDL2 REQUIRED)
include_directories(${OPENGL_INCLUDE_DIRS}  ${GLEW_INCLUDE_DIRS} ${SDL2_INCLUDE_DIRS})

target_link_libraries(04_StencilMiroir ${OPENGL_LIBRARIES} ${GLEW_LIBRARIES} ${SDL2_LIBRARIES})
