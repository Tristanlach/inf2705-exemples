#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint prog = -1;
GLint locVertex = -1;
GLint locColor = -1;
GLint locmatrModel = -1;
GLint locmatrVisu = -1;
GLint locmatrProj = -1;
GLint locavecClip = -1;
GLint locplanCoupe = -1;

GLuint vao[2];
GLuint vbo[3];

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
