Touches possibles :
    q:  Quitter l'application
    v:  Recharger les nuanceurs
    g:  Permuter l'affichage en fil de fer ou plein
    GAUCHE:  Décrémenter le type de flou
    DROITE:  Incrémenter le type de flou
    s:  Sauvegarder une copie de la fenêtre dans un fichier
