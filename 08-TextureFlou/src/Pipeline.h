#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint prog;
GLint locVertex;
GLint locTexCoord;
GLint locmatrModel;
GLint locmatrVisu;
GLint locmatrProj;
GLint loclaTextureScene;
GLint loctaillecour;
GLint locposcour;
GLint loctypeFlou;

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
