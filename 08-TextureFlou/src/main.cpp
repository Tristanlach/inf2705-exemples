#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "inf2705-texture.h"
#include "Etat.h"
#include "Pipeline.h"

void chargerTextures()
{
    GLuint maTextureScene=0;

    unsigned char *pixels;
    GLsizei largeur, hauteur;
    if ( ( pixels = ChargerImage( "textures/scene.bmp", largeur, hauteur ) ) != NULL )
    {
        glGenTextures( 1, &maTextureScene );
        glBindTexture( GL_TEXTURE_2D, maTextureScene );
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, largeur, hauteur, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glBindTexture( GL_TEXTURE_2D, 0 );
        delete[] pixels;
    }

    // assigner chaque image dans une unité de texture différente
    glActiveTexture( GL_TEXTURE0 ); // l'unité de texture 0
    glBindTexture( GL_TEXTURE_2D, maTextureScene );
}

void chargerNuanceurs()
{
    // créer le programme
    prog = glCreateProgram();

    // attacher le nuanceur de sommets
    const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesSommets[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
        glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesSommets[1];
    }
    // attacher le nuanceur de fragments
    const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesFragments[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
        glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesFragments[1];
    }
    // faire l'édition des liens du programme
    glLinkProgram( prog );
    ProgNuanceur::afficherLogLink( prog );

    // demander la "Location" des variables
    if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
    if ( ( locTexCoord = glGetAttribLocation( prog, "TexCoord" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de TexCoord" << std::endl;
    if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
    if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
    if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
    if ( ( loclaTextureScene = glGetUniformLocation( prog, "laTextureScene" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de laTextureScene" << std::endl;
    if ( ( locposcour = glGetUniformLocation( prog, "poscour" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de poscour" << std::endl;
    if ( ( loctaillecour = glGetUniformLocation( prog, "taillecour" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de taillecour" << std::endl;
    if ( ( loctypeFlou = glGetUniformLocation( prog, "typeFlou" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de typeFlou" << std::endl;

    FenetreTP::VerifierErreurGL("fin chargerNuanceurs");
}

void FenetreTP::initialiser()
{
    // charger les textures
    chargerTextures();

    // charger les nuanceurs (tous vos fichiers de nuanceurs seront chargés ici)
    chargerNuanceurs();

    // Le modèle
    const GLfloat coords[] = { -1.0, -1.0,   1.0, -1.0,   1.0,  1.0,   -1.0,  1.0 };
    const GLfloat texcoo[] = {  0.0,  0.0,   1.0,  0.0,   1.0,  1.0,    0.0,  1.0 };
    const GLuint connec[] = {  0, 1, 2,   2, 3, 0  };

    // allouer les objets OpenGL
    GLuint vao[1];  glGenVertexArrays( 1, vao );
    GLuint vbo[3];  glGenBuffers( 3, vbo );

    // initialiser le VAO
    glBindVertexArray( vao[0] );

    // charger le VBO pour les sommets
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(coords), coords, GL_STATIC_DRAW );
    glVertexAttribPointer( locVertex, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locVertex);

    // charger le VBO pour la connectivité
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vbo[1] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(connec), connec, GL_STATIC_DRAW );

    // charger le VBO pour les coordonnées de texture
    glBindBuffer( GL_ARRAY_BUFFER, vbo[2] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(texcoo), texcoo, GL_STATIC_DRAW );
    glVertexAttribPointer( locTexCoord, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    glEnableVertexAttribArray(locTexCoord);
}

void FenetreTP::afficherScene()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    // assigner les variables uniformes
    glUseProgram( prog );
    glUniform1i( loclaTextureScene, 0 ); // unité de texture 0
    glUniform2iv( locposcour, 1, glm::value_ptr(Etat::poscour) );
    glUniform2iv( loctaillecour, 1, glm::value_ptr(Etat::taillecour) );
    glUniform1i( loctypeFlou, Etat::typeFlou );

    // transformation de projection orthogonale
    matrProj.Ortho( -1.0, 1.0, -1.0, 1.0, -1.0, 1.0 );
    glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

    // transformation de visualisation
    matrVisu.LookAt( 0.0, 0.0, 0.0,  0.0, 0.0, -1.0,  0.0, 1.0, 0.0 );
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    // transformation de modélisation
    matrModel.LoadIdentity();
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );

    // afficher les deux triangles qui forment le carré (6 indices dans connec[])
    glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 );

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
    Etat::taillecour = glm::ivec2(w,h);
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_g: // Permuter l'affichage en fil de fer ou plein
        {
            GLint modePlein[2];
            glGetIntegerv( GL_POLYGON_MODE, modePlein );
            glPolygonMode( GL_FRONT_AND_BACK, ( modePlein[0] == GL_LINE ) ? GL_FILL : GL_LINE );
        }
        break;

    case TP_GAUCHE: // Décrémenter le type de flou
        if ( --Etat::typeFlou < 0 ) Etat::typeFlou = 3;
        break;

    case TP_DROITE: // Incrémenter le type de flou
        if ( ++Etat::typeFlou > 3 ) Etat::typeFlou = 0;
        break;

    case TP_s: // Sauvegarder une copie de la fenêtre dans un fichier
        sauvegarderFenetre( );
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        break;
    }
}

static bool presse = false;
void FenetreTP::sourisClic( int button, int state, int x, int y )
{ presse = ( state == TP_PRESSE ); }

void FenetreTP::sourisMolette( int x, int y )
{
}

void FenetreTP::sourisMouvement( int x, int y )
{ if ( presse ) Etat::poscour = glm::ivec2(x,hauteur_-y); }

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "texture", 500, 500 );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }
    return 0;
}
