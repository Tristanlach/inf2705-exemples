#ifndef __ETAT_H__
#define __ETAT_H__

#include "inf2705-Singleton.h"

//
// variables d'état
//
class Etat : public Singleton<Etat>
{
    SINGLETON_DECLARATION_CLASSE(Etat);
public:
    static bool afficheAxes; // indique si on affiche les axes
    static bool afficheNormales;
};

#endif
