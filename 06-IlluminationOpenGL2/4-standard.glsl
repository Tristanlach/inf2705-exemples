/////////////////////////
// Nuanceur de sommets //
/////////////////////////

void directionnelle( in int i,
                     in vec3 normal,
                     inout vec4 ambient,
                     inout vec4 diffuse,
                     inout vec4 specular )
{
    float NdotL;  // normale . direction de la lumière
    float NdotHV; // normale . demi-vecteur de la lumière
    float exp;    // exposant spéculaire

    // (En OpenGl 2.x, gl_LightSource[i].position est déjà dans le repère de la caméra)
    NdotL = max( 0.0, dot( normal, gl_LightSource[i].position.xyz ) );
    NdotHV = max( 0.0, dot( normal, gl_LightSource[i].halfVector.xyz ) );

    exp = ( NdotL == 0.0 ) ? 0.0 : pow( NdotHV, gl_FrontMaterial.shininess );
    ambient  += gl_LightSource[i].ambient;
    diffuse  += gl_LightSource[i].diffuse * NdotL;
    specular += gl_LightSource[i].specular * exp;
}

void ponctuelle( in int i,
                 in vec3 normal,
                 in vec3 obsVec,
                 in vec3 pos,
                 inout vec4 ambient,
                 inout vec4 diffuse,
                 inout vec4 specular )
{
    float NdotL;       // normale . direction de la lumière
    float NdotHV;      // normale . demi-vecteur de la lumière
    float exp;         // exposant spéculaire
    float attenuation; // facteur d'atténuation
    float d;           // distance de la surface à la source lumineuse
    vec3 L;            // direction de la surface vers la source lumineuse
    vec3 halfVector;   // direction du reflet spéculaire maximum

    // calcul du vecteur de la surface vers la source lumineuse (En OpenGl 2.x, gl_LightSource[i].position est déjà dans le repère de la caméra)
    L = vec3( gl_LightSource[i].position ) / gl_LightSource[i].position.w - pos;

    // calculer la distance de la surface à la source lumineuse
    d = length( L );

    // normaliser le vecteur de la surface vers la source lumineuse
    L = normalize( L );

    // calculer l'atténuation selon la distance à l'objet
    attenuation = 1.0 / ( gl_LightSource[i].constantAttenuation +
                          gl_LightSource[i].linearAttenuation * d +
                          gl_LightSource[i].quadraticAttenuation * d * d );
    //attenuation = 1.0;

    // calculer les divers produits scalaires pour l'illumination
    NdotL = max( 0.0, dot( normal, L ) );
    //halfVector = normalize( L + obsVec ); NdotHV = max( 0.0, dot( normal, halfVector ) );
    NdotHV = max( 0.0, dot( normal, gl_LightSource[i].halfVector.xyz ) );

    // exposant spéculaire
    exp = ( NdotL == 0.0 ) ? 0.0 : pow( NdotHV, gl_FrontMaterial.shininess );

    // additionner les réflexions calculées
    ambient  += gl_LightSource[i].ambient;
    diffuse  += attenuation * gl_LightSource[i].diffuse * NdotL;
    specular += attenuation * gl_LightSource[i].specular * exp;
}

void spot( in int i,
           in vec3 normal,
           in vec3 obsVec,
           in vec3 pos,
           inout vec4 ambient,
           inout vec4 diffuse,
           inout vec4 specular )
{
    float NdotL;           // normale . direction de la lumière
    float NdotHV;          // normale . demi-vecteur de la lumière
    float exp;             // exposant spéculaire
    float spotDot;         // cosinus de l'angle du spot
    float spotAttenuation; // facteur d'atténuation du spot
    float attenuation;     // facteur d'atténuation
    float d;               // distance de la surface à la source lumineuse
    vec3 L;                // direction de la surface vers la source lumineuse
    vec3 halfVector;       // direction du reflet spéculaire maximum

    // calcul du vecteur de la surface vers la source lumineuse (En OpenGl 2.x, gl_LightSource[i].position est déjà dans le repère de la caméra)
    L = vec3( gl_LightSource[i].position ) - pos;

    // calculer la distance de la surface à la source lumineuse
    d = length( L );

    // normaliser le vecteur de la surface vers la source lumineuse
    L = normalize( L );

    // calculer l'atténuation selon la distance à l'objet
    attenuation = 1.0 / ( gl_LightSource[i].constantAttenuation +
                          gl_LightSource[i].linearAttenuation * d +
                          gl_LightSource[i].quadraticAttenuation * d * d );
    //attenuation = 1.0;

    // voir si le point éclairé est dans le cône d'illumination du spot
    spotDot = dot( -L, normalize( gl_LightSource[i].spotDirection ) ); // (En OpenGl 2.x, gl_LightSource[i].spotDirection est déjà dans le repère de la caméra)

    spotAttenuation = ( spotDot < gl_LightSource[i].spotCosCutoff ) ? 0.0 : pow( spotDot, gl_LightSource[i].spotExponent );

    // combiner l'effet spot et l'atténuation due à la distance
    attenuation *= spotAttenuation;

    // calculer les divers produits scalaires pour l'illumination
    NdotL = max( 0.0, dot( normal, L ) );
    //halfVector = normalize( L + obsVec ); NdotHV = max( 0.0, dot( normal, halfVector ) );
    NdotHV = max( 0.0, dot( normal, gl_LightSource[i].halfVector.xyz ) );

    // calculer l'exposant spéculaire
    exp = ( NdotL == 0.0 ) ? 0.0 : pow( NdotHV, gl_FrontMaterial.shininess );

    // additionner les réflexions calculées
    ambient  += gl_LightSource[i].ambient;
    diffuse  += attenuation * gl_LightSource[i].diffuse * NdotL;
    specular += attenuation * gl_LightSource[i].specular * exp;
}

void main( void )
{
    const int NumEnabledLights = 3;
    const bool LocalViewer = false;
    const bool TwoSidedLighting = false;

    // appliquer la transformation standard du sommet (ModelView et Projection)
    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
    // ou gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;

    // calculer la position du sommet en coordonnées de la caméra (repère de l'observateur)
    vec3 pos = vec3( gl_ModelViewMatrix * gl_Vertex );
    // déterminer si on doit calculer en mode "LocalViewer"
    vec3 obsVec;
    // calculer le vecteur de la direction vers l'observateur
    if ( LocalViewer )
        obsVec = normalize( -pos ); // (=0-pos) un vecteur qui pointe vers le (0,0,0), c'est-à-dire vers la caméra
    else
        obsVec = vec3( 0.0, 0.0, 1.0 ); // on considère que l'observateur (la caméra) est à l'infini dans la direction (0,0,1)

    // calculer la normale
    vec3 normal = normalize( gl_NormalMatrix * gl_Normal );

    // initialiser les accumulateurs pour la couleur
    vec4 ambient  = vec4( 0.0 );
    vec4 diffuse  = vec4( 0.0 );
    vec4 specular = vec4( 0.0 );

    // pour chaque lumière, calculer ...
    int i;
    for ( i = 0 ; i < NumEnabledLights ; ++i )
    {
        // la direction de la lumière (En OpenGl 2.x, gl_LightSource[i].position est déjà dans le repère de la caméra)
        vec3 lumiDir = normalize(vec3(gl_LightSource[i].position));
        // son demi-vecteur normalisée
        vec3 halfVec = normalize(vec3(gl_LightSource[i].halfVector));
        // doit-on inverser la normale?
        vec3 viewDir = halfVec - lumiDir;
        bool reverse = ( dot(normal, viewDir) < 0.0 );
        // faire le calcul spécifique à chaque type de source lumineuse
        if ( gl_LightSource[i].position.w == 0.0 )
        {
            directionnelle( i, reverse ? -normal : normal, ambient, diffuse, specular );
        }
        else if ( gl_LightSource[i].spotCutoff == 180.0 )
        {
            ponctuelle( i, reverse ? -normal : normal, obsVec, pos, ambient, diffuse, specular );
        }
        else
        {
            spot( i, reverse ? -normal : normal, obsVec, pos, ambient, diffuse, specular );
        }
    }

    // calculer la couleur totale au sommet
    vec4 couleur = gl_FrontMaterial.emission + gl_FrontMaterial.ambient * gl_LightModel.ambient;
    // ou couleur = gl_FrontLightModelProduct.sceneColor;
    couleur += gl_FrontMaterial.ambient * ambient;
    couleur += gl_FrontMaterial.diffuse * diffuse;
    couleur += gl_FrontMaterial.specular * specular;

    gl_FrontColor = clamp( couleur, 0.0, 1.0 );
}

///////////////////////////
// Nuanceur de fragments //
///////////////////////////

void main (void)
{
    gl_FragColor = gl_Color;
}
