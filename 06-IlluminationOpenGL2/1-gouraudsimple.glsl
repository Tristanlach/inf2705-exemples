/////////////////////////
// Nuanceur de sommets //
/////////////////////////

void main(void)
{
    // appliquer la transformation standard du sommet (ModelView et Projection)
    gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
    // ou gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // calculer la normale normalisée
    vec3 N = normalize( gl_NormalMatrix * gl_Normal );

    // normaliser le demi-vecteur (Blinn)
    vec3 halfV = normalize( vec3(gl_LightSource[0].halfVector) );
    // calculer le produit scalaire pour la réflexion spéculaire
    float NdotHV = max( dot(N,halfV), 0.0 );

    // calculer composante diffuse (En OpenGl 2.x, gl_LightSource[i].position est déjà dans le repère de la caméra)
    gl_FrontColor = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse * abs(dot(N,vec3(gl_LightSource[0].position)));
    // calculer composante spéculaire
    gl_FrontColor += gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(NdotHV, gl_FrontMaterial.shininess);
    // calculer composante ambiante
    gl_FrontColor += gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
}

///////////////////////////
// Nuanceur de fragments //
///////////////////////////

void main(void)
{
    // la couleur du fragment est la couleur interpolée en appliquant la méthode de Gouraud
    gl_FragColor = gl_Color;
}
