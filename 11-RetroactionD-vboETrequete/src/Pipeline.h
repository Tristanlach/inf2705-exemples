#ifndef __PIPELINE_H__
#define __PIPELINE_H__

// variables pour l'utilisation des nuanceurs
GLuint prog;
GLint locVertex;
GLint locColor;
GLint locmatrModel;
GLint locmatrVisu;
GLint locmatrProj;
GLuint progRetroaction;
GLint locVertexRetroaction;
GLint locColorRetroaction;
GLint locdt;

GLuint vao[2];   // deux VAO
GLuint vbo[2];   // les VBO de cet exemple
GLuint tfo[1];   // les TFO de cet exemple
GLuint requete;

// matrices du pipeline graphique
MatricePipeline matrModel, matrVisu, matrProj;

#endif
