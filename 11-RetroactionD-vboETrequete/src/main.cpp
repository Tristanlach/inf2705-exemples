#include <stdlib.h>
#include <iostream>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "Etat.h"
#include "Pipeline.h"

struct Sommet
{
    glm::vec4 position; // les coordonnées du sommet
    glm::vec4 couleur;  // les couleurs du sommet
    GLfloat   autre1;   // autre valeur
    glm::vec4 autre2;   // autre tableau de valeurs
};
static Sommet triangle[3] = // un tableau de sommets pour tracer le triangle
{
    { { 0.0,  1.0, 0.0, 1.0 }, { 1.0, 0.0, 0.0, 1.0 }, 1.0, { 1, 0, 0, 1 } },
    { {-1.0, -1.0, 0.0, 1.0 }, { 0.0, 1.0, 0.0, 1.0 }, 0.0, { 0, 1, 0, 1 } },
    { { 1.0, -1.0, 0.0, 1.0 }, { 0.0, 0.0, 1.0, 1.0 }, 0.0, { 0, 0, 1, 1 } }
};

GLfloat dt = 0.01;

void calculerPhysique( )
{
    // ajuster le dt selon la fréquence d'affichage
    {
        static int tempsPrec = 0;
        // obtenir le temps depuis l'initialisation (en millisecondes)
        int tempsCour = FenetreTP::obtenirTemps();
        // calculer un nouveau dt (sauf la première fois)
        if ( tempsPrec ) dt = ( tempsCour - tempsPrec )/1000.0;
        // se préparer pour la prochaine fois
        tempsPrec = tempsCour;
    }

    if ( Etat::enmouvement )
    {
        // déplacer en utilisant le nuanceur de rétroaction
        glUseProgram( progRetroaction );
        glUniform1f( locdt, dt );

        // dire où seront stockés les résultats
        glBindBufferBase( GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[1] );

        glBindVertexArray( vao[1] );         // pour les résultats de la rétroaction, sélectionner le second VAO
        // se préparer
        glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );  // utiliser les valeurs courantes
        glVertexAttribPointer( locVertexRetroaction, 4, GL_FLOAT, GL_FALSE, sizeof(Sommet), reinterpret_cast<void*>( offsetof(Sommet,position) ) );
        glVertexAttribPointer( locColorRetroaction, 4, GL_FLOAT, GL_FALSE, sizeof(Sommet), reinterpret_cast<void*>( offsetof(Sommet,couleur) ) );

        // débuter la requête
        if ( Etat::impression )
            glBeginQuery( GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, requete );

        // désactiver le tramage
        glEnable( GL_RASTERIZER_DISCARD );
        // débuter la rétroaction
        glBeginTransformFeedback( GL_TRIANGLES );
        // « dessiner » (en utilisant le vbo[0])
        glDrawArrays( GL_TRIANGLES, 0, sizeof(triangle)/sizeof(Sommet) );
        // terminer la rétroaction
        glEndTransformFeedback();
        // réactiver le tramage
        glDisable( GL_RASTERIZER_DISCARD );

        // terminer la requête
        if ( Etat::impression )
            glEndQuery( GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN );

        glBindVertexArray( 0 );              // désélectionner le VAO

        if ( Etat::impression )
        {
            glFlush(); // attendre que la carte grapohique ait terminé le traitement
            // obtenir et imprimer les résultats
            GLuint nresul;
            glGetQueryObjectuiv( requete, GL_QUERY_RESULT, &nresul );

            struct Sommet tamponRetour[sizeof(triangle)/sizeof(Sommet)];
            glGetBufferSubData( GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(tamponRetour), tamponRetour );

            std::cout << " nresul=" << nresul << std::endl;
            for ( unsigned int i = 0; i < 3*nresul; ++i )
                std::cout << "   tamponRetour["<<i<<"].position[]="
                          << " " << glm::to_string(tamponRetour[i].position)
                          << "   tamponRetour["<<i<<"].couleur[]="
                          << " " << glm::to_string(tamponRetour[i].couleur)
                          << std::endl;

            Etat::impression = false;
        }

        // échanger les numéros des deux VBO
        std::swap( vbo[0], vbo[1] );

        FenetreTP::VerifierErreurGL("calculerPhysique");
    }
}

void chargerNuanceurs()
{
    {
        // créer le programme
        prog = glCreateProgram();

        // attacher le nuanceur de sommets
        const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesSommets[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
            glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( prog, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesSommets[1];
        }
        // attacher le nuanceur de fragments
        const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesFragments[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
            glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( prog, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesFragments[1];
        }
        // faire l'édition des liens du programme
        glLinkProgram( prog );
        ProgNuanceur::afficherLogLink( prog );

        // demander la "Location" des variables
        if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
        if ( ( locColor = glGetAttribLocation( prog, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
        if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
        if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
        if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
    }

    {
        // créer le programme
        progRetroaction = glCreateProgram();

        // attacher le nuanceur de sommets
        const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS_RETROACTION\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
        if ( chainesSommets[1] != NULL )
        {
            GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
            glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
            glCompileShader( nuanceurObj );
            glAttachShader( progRetroaction, nuanceurObj );
            ProgNuanceur::afficherLogCompile( nuanceurObj );
            delete [] chainesSommets[1];
        }

        // dire quelles variables on veut récupérer
        const GLchar* vars[] = { "VertexMod", "ColorMod", "gl_SkipComponents1", "gl_SkipComponents4"  };
        glTransformFeedbackVaryings( progRetroaction, sizeof(vars)/sizeof(vars[0]), vars, GL_INTERLEAVED_ATTRIBS );

        // faire l'édition des liens du programme
        glLinkProgram( progRetroaction );
        ProgNuanceur::afficherLogLink( progRetroaction );

        // demander la "Location" des variables
        if ( ( locVertexRetroaction = glGetAttribLocation( progRetroaction, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
        if ( ( locColorRetroaction = glGetAttribLocation( progRetroaction, "Color" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Color" << std::endl;
        if ( ( locdt = glGetUniformLocation( progRetroaction, "dt" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de dt" << std::endl;
    }
}

void FenetreTP::initialiser()
{
    // couleur de l'arrière-plan
    glm::vec4 couleurFond( 0.2, 0.2, 0.2, 1.0 );
    glClearColor( couleurFond.r, couleurFond.g, couleurFond.b, couleurFond.a );

    // charger les nuanceurs
    chargerNuanceurs();

    FenetreTP::VerifierErreurGL("debut initialiser");

    // Charger le modèle

    // allouer les objets OpenGL
    glGenVertexArrays( 2, vao );
    glGenBuffers( 2, vbo );
    glGenTransformFeedbacks( 1, tfo );

    // charger le VBO pour les valeurs modifiés
    glBindTransformFeedback( GL_TRANSFORM_FEEDBACK, tfo[0] );
    glBindBuffer( GL_ARRAY_BUFFER, vbo[1] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(triangle), NULL, GL_STREAM_READ ); // on ne donne rien sinon la taille

    // charger le VBO pour le triangle
    glBindVertexArray( vao[0] );
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STREAM_DRAW );
    glVertexAttribPointer( locVertex, 4, GL_FLOAT, GL_FALSE, sizeof(Sommet), reinterpret_cast<void*>( offsetof(Sommet,position) ) );
    glEnableVertexAttribArray(locVertex);
    glVertexAttribPointer( locColor, 4, GL_FLOAT, GL_FALSE, sizeof(Sommet), reinterpret_cast<void*>( offsetof(Sommet,couleur) ) );
    glEnableVertexAttribArray(locColor);
    glBindVertexArray( 0 );

    // charger le VBO pour les valeurs modifiées
    glBindVertexArray( vao[1] );
    glBindBuffer( GL_ARRAY_BUFFER, vbo[1] );
    glBufferData( GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STREAM_READ ); // déjà fait ci-dessus
    glVertexAttribPointer( locVertexRetroaction, 4, GL_FLOAT, GL_FALSE, sizeof(Sommet), reinterpret_cast<void*>( offsetof(Sommet,position) ) );
    glEnableVertexAttribArray(locVertexRetroaction);
    glVertexAttribPointer( locColorRetroaction, 4, GL_FLOAT, GL_FALSE, sizeof(Sommet), reinterpret_cast<void*>( offsetof(Sommet,couleur) ) );
    glEnableVertexAttribArray(locColorRetroaction);
    glBindVertexArray( 0 );

    // Défaire tous les liens
    glBindBuffer( GL_ARRAY_BUFFER, 0 );

    // créer la requête afin d'obtenir un retour d'information
    glGenQueries( 1, &requete );

    FenetreTP::VerifierErreurGL("fin initialiser");
}

void FenetreTP::conclure()
{
    glUseProgram( 0 );
    glDeleteVertexArrays( 2, vao );
    glDeleteBuffers( 2, vbo );
    glDeleteQueries( 1, &requete );
}

void FenetreTP::afficherScene()
{
    // effacer l'ecran et le tampon de profondeur
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glUseProgram( prog );

    // mettre à jour la projection
    // matrProj.Ortho( -2, 2, -2, 2, 1.0, 5.0 );
    // glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );
    GLdouble aspect = (GLdouble) largeur_ / (GLdouble) hauteur_;
    matrProj.Perspective( 25.0, aspect, 1.0, 30.0 );
    glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

    // mettre à jour la caméra
    matrVisu.LookAt( 0, 0, 10,  0.0, 0.0, 0.0,  0.0, 1.0, 0.0 );
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    // afficher le modèle
    matrModel.LoadIdentity();
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );

    // afficher le triangle
    glBindVertexArray( vao[0] );         // pour l'affichage, sélectionner le premier VAO
    // refaire le lien avec les attributs du nuanceur de sommets pour le vbo actuellement utilisé
    glBindBuffer( GL_ARRAY_BUFFER, vbo[0] );
    glVertexAttribPointer( locVertex, 4, GL_FLOAT, GL_FALSE, sizeof(Sommet), reinterpret_cast<void*>( offsetof(Sommet,position) ) );
    glVertexAttribPointer( locColor, 4, GL_FLOAT, GL_FALSE, sizeof(Sommet), reinterpret_cast<void*>( offsetof(Sommet,couleur) ) );
    //glDrawTransformFeedback( GL_TRIANGLES, tfo[0] ); // une autre possibilité
    glDrawArrays( GL_TRIANGLES, 0, sizeof(triangle)/sizeof(Sommet) );
    glBindVertexArray( 0 );              // désélectionner le VAO

    FenetreTP::VerifierErreurGL("fin afficherScene");

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;

    case TP_v: // Recharger les nuanceurs
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_g: // Permuter l'affichage en fil de fer ou plein
        {
            GLint modePlein[2];
            glGetIntegerv( GL_POLYGON_MODE, modePlein );
            glPolygonMode( GL_FRONT_AND_BACK, ( modePlein[0] == GL_LINE ) ? GL_FILL : GL_LINE );
        }
        break;

    case TP_i: // Faire une impression
        Etat::impression = true;
        break;

    case TP_ESPACE: // Permuter la rotation automatique du modèle
        Etat::enmouvement = !Etat::enmouvement;
        break;

    case TP_s: // Sauvegarder une copie de la fenêtre dans un fichier
        sauvegarderFenetre( );
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        break;
    }
}

void FenetreTP::sourisClic( int button, int state, int x, int y )
{
}

void FenetreTP::sourisMolette( int x, int y )
{
}

void FenetreTP::sourisMouvement( int x, int y )
{
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "retro-impression" );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // mettre à jour la physique
        calculerPhysique( );

        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
