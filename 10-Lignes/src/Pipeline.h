#ifndef __PIPELINE_H__
#define __PIPELINE_H__

GLuint prog;
GLint locVertex;
GLint locColor;
GLint locmatrModel;
GLint locmatrVisu;
GLint locmatrProj;
GLint loclaTexture;
GLint loctexnumero;
GLint locpointsize;

GLuint vao[1];
GLuint vbo[2];

#endif
