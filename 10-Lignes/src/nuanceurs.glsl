// "#version 4xx\n#define NUANCEUR_...\n" doit être ajouté en préambule au chargement de chaque nuanceur
////////////////////////////////////////////////////////////////////////////////
#if defined(NUANCEUR_SOMMETS)

// out gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

uniform mat4 matrModel;
uniform mat4 matrVisu;
uniform mat4 matrProj;
uniform float pointsize;

layout(location=0) in vec4 Vertex;
layout(location=3) in vec4 Color;

out Attribs {
    vec4 couleur;
} AttribsOut;

void main( void )
{
    // appliquer la transformation standard du sommet
    gl_Position = matrProj * matrVisu * matrModel * Vertex;

    // couleur du sommet
    AttribsOut.couleur = Color;

    // assigner la taille des points (en pixels)
    gl_PointSize = pointsize;
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_GEOMETRIE_POINTS)

layout(points) in;
layout(points, max_vertices = 1) out;

// in gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_in[];

// out gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

in Attribs {
    vec4 couleur;
} AttribsIn[];

out Attribs {
    vec4 couleur;
} AttribsOut;

uniform mat4 matrProj;

void main()
{
    // assigner la position du point
    gl_Position = gl_in[0].gl_Position;

    // assigner la taille des points (en pixels)
    gl_PointSize = gl_in[0].gl_PointSize;

    // assigner les autres attributs
    AttribsOut.couleur = AttribsIn[0].couleur;
    EmitVertex();
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_GEOMETRIE_LINES)

layout(points) in;
// on produit des lignes, 2 sommets par lignes, max 2 sommets par segment de droite
layout(line_strip, max_vertices = 2) out;

// in gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// } gl_in[];

// out gl_PerVertex // <-- déclaration implicite
// {
//   vec4 gl_Position;
//   float gl_PointSize;
//   float gl_ClipDistance[];
// };

in Attribs {
    vec4 couleur;
} AttribsIn[];

out Attribs {
    vec4 couleur;
} AttribsOut;

uniform mat4 matrProj;

void main()
{
    // le premier sommet
    gl_Position = gl_in[0].gl_Position;
    // émettre le premier sommet de la primitive EmitVertex();
    // le second sommet déplacé selon x
    gl_Position = gl_in[0].gl_Position; gl_Position.x += 0.2; // émettre le second sommet de la primitive

    // assigner les autres attributs
    AttribsOut.couleur = AttribsIn[0].couleur;
    EmitVertex();
}

////////////////////////////////////////////////////////////////////////////////
#elif defined(NUANCEUR_FRAGMENTS)

// in vec4 gl_FragCoord;   // <-- déclaration implicite
// in bool gl_FrontFacing; // <-- déclaration implicite
// in vec2 gl_PointCoord;  // <-- déclaration implicite
// out float gl_FragDepth; // <-- déclaration implicite

uniform sampler2D laTexture;
uniform int texnumero;

in Attribs {
    vec4 couleur;
} AttribsIn;

out vec4 FragColor;

void main(void)
{
    FragColor = AttribsIn.couleur;
}

#endif
