#include <stdlib.h>
#include <iostream>
#include <math.h>
#include "inf2705-matrice.h"
#include "inf2705-nuanceur.h"
#include "inf2705-fenetre.h"
#include "inf2705-forme.h"
#include "Etat.h"
#include "Pipeline.h"
#include "Camera.h"

// les formes
FormeTheiere *theiere = NULL;

// définition de la lumière
struct LightSourceParameters
{
    glm::vec4 ambient;
    glm::vec4 diffuse;
    glm::vec4 specular;
    glm::vec4 position;       // dans le repère du monde (il faudra convertir vers le repère de la caméra pour les calculs)
    glm::vec3 spotDirection;  // dans le repère du monde (il faudra convertir vers le repère de la caméra pour les calculs)
    float spotExposant;
    float spotAngleOuverture; // angle d'ouverture delta du spot ([0.0,90.0] ou 180.0)
    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;
} LightSource = { glm::vec4( 1.0, 1.0, 1.0, 1.0 ),
                  glm::vec4( 1.0, 1.0, 1.0, 1.0 ),
                  glm::vec4( 1.0, 1.0, 1.0, 1.0 ),
                  glm::vec4( 0.0, 0.0, 30.0, 1.0 ),
                  glm::vec3( 0.0, 0.0, -1.0 ),
                  1.0,       // l'exposant du cône
                  20.0,      // l'angle du cône du spot
                  1., 0., 0. // atténuation
};

// définition du matériau
struct MaterialParameters
{
    glm::vec4 emission;
    glm::vec4 ambient;
    glm::vec4 diffuse;
    glm::vec4 specular;
    float shininess;
} FrontMaterial = { glm::vec4( 0.0, 0.0, 0.0, 1.0 ),
                    glm::vec4( 0.2, 0.2, 0.2, 1.0 ),
                    glm::vec4( 1.0, 1.0, 1.0, 1.0 ),
                    glm::vec4( 1.0, 1.0, 1.0, 1.0 ),
                    20.0 };

struct LightModelParameters
{
    glm::vec4 ambient; // couleur ambiante globale
    int localViewer;   // doit-on prendre en compte la position de l'observateur? (local ou à l'infini)
    int twoSide;       // éclairage sur les deux côtés ou un seul?
} LightModel = { glm::vec4(0,0,0,1), false, false };

void calculerPhysique( )
{
}

void chargerNuanceurs()
{
    // créer le programme
    prog = glCreateProgram();

    // attacher le nuanceur de sommets
    const GLchar *chainesSommets[2] = { "#version 410\n#define NUANCEUR_SOMMETS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesSommets[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_VERTEX_SHADER );
        glShaderSource( nuanceurObj, 2, chainesSommets, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesSommets[1];
    }
    // attacher le nuanceur de fragments
    const GLchar *chainesFragments[2] = { "#version 410\n#define NUANCEUR_FRAGMENTS\n", ProgNuanceur::lireNuanceur( "nuanceurs.glsl" ) };
    if ( chainesFragments[1] != NULL )
    {
        GLuint nuanceurObj = glCreateShader( GL_FRAGMENT_SHADER );
        glShaderSource( nuanceurObj, 2, chainesFragments, NULL );
        glCompileShader( nuanceurObj );
        glAttachShader( prog, nuanceurObj );
        ProgNuanceur::afficherLogCompile( nuanceurObj );
        delete [] chainesFragments[1];
    }
    // faire l'édition des liens du programme
    glLinkProgram( prog );
    ProgNuanceur::afficherLogLink( prog );

    // demander la "Location" des variables
    if ( ( locVertex = glGetAttribLocation( prog, "Vertex" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Vertex" << std::endl;
    if ( ( locNormal = glGetAttribLocation( prog, "Normal" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de Normal (partie 1)" << std::endl;
    if ( ( locmatrModel = glGetUniformLocation( prog, "matrModel" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrModel" << std::endl;
    if ( ( locmatrVisu = glGetUniformLocation( prog, "matrVisu" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrVisu" << std::endl;
    if ( ( locmatrProj = glGetUniformLocation( prog, "matrProj" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrProj" << std::endl;
    if ( ( locmatrNormale = glGetUniformLocation( prog, "matrNormale" ) ) == -1 ) std::cerr << "!!! pas trouvé la \"Location\" de matrNormale (partie 1)" << std::endl;
    if ( ( indLightSource = glGetUniformBlockIndex( prog, "LightSourceParameters" ) ) == GL_INVALID_INDEX ) std::cerr << "!!! pas trouvé l'\"index\" de LightSource" << std::endl;
    if ( ( indFrontMaterial = glGetUniformBlockIndex( prog, "MaterialParameters" ) ) == GL_INVALID_INDEX ) std::cerr << "!!! pas trouvé l'\"index\" de FrontMaterial" << std::endl;
    if ( ( indLightModel = glGetUniformBlockIndex( prog, "LightModelParameters" ) ) == GL_INVALID_INDEX ) std::cerr << "!!! pas trouvé l'\"index\" de LightModel" << std::endl;

    // charger les ubo
    {
        glBindBuffer( GL_UNIFORM_BUFFER, ubo[0] );
        glBufferData( GL_UNIFORM_BUFFER, sizeof(LightSource), &LightSource, GL_DYNAMIC_COPY );
        glBindBuffer( GL_UNIFORM_BUFFER, 0 );
        const GLuint bindingIndex = 0;
        glBindBufferBase( GL_UNIFORM_BUFFER, bindingIndex, ubo[0] );
        glUniformBlockBinding( prog, indLightSource, bindingIndex );
    }
    {
        glBindBuffer( GL_UNIFORM_BUFFER, ubo[1] );
        glBufferData( GL_UNIFORM_BUFFER, sizeof(FrontMaterial), &FrontMaterial, GL_DYNAMIC_COPY );
        glBindBuffer( GL_UNIFORM_BUFFER, 0 );
        const GLuint bindingIndex = 1;
        glBindBufferBase( GL_UNIFORM_BUFFER, bindingIndex, ubo[1] );
        glUniformBlockBinding( prog, indFrontMaterial, bindingIndex );
    }
    {
        glBindBuffer( GL_UNIFORM_BUFFER, ubo[2] );
        glBufferData( GL_UNIFORM_BUFFER, sizeof(LightModel), &LightModel, GL_DYNAMIC_COPY );
        glBindBuffer( GL_UNIFORM_BUFFER, 0 );
        const GLuint bindingIndex = 2;
        glBindBufferBase( GL_UNIFORM_BUFFER, bindingIndex, ubo[2] );
        glUniformBlockBinding( prog, indLightModel, bindingIndex );
    }
    glBindBuffer( GL_UNIFORM_BUFFER, 0 );
}

void FenetreTP::initialiser()
{
    glEnable( GL_DEPTH_TEST );
    glm::vec4 couleurFond( 0.2, 0.2, 0.2, 1 );
    glClearColor( couleurFond.r, couleurFond.g, couleurFond.b, couleurFond.a );

    // allouer les UBO pour les variables uniformes
    glGenBuffers( 3, ubo );

    // charger les nuanceurs
    chargerNuanceurs();
    glUseProgram( prog );

    FenetreTP::VerifierErreurGL("debut initialiser");

    // créer quelques autres formes
    glUseProgram( prog );
    theiere = new FormeTheiere( );

    FenetreTP::VerifierErreurGL("fin initialiser");
}

void FenetreTP::conclure()
{
    glUseProgram( 0 );
    glDeleteVertexArrays( 1, vao );
    glDeleteBuffers( 4, vbo );
    glDeleteBuffers( 3, ubo );
    delete theiere;
}

static void afficherTheiere( glm::vec2 pos, glm::vec3 ambi, glm::vec3 diff, glm::vec3 spec, float shin )
{
    // mettre à jour les blocs de variables uniformes
    FrontMaterial.ambient = glm::vec4(ambi,1);
    FrontMaterial.diffuse = glm::vec4(diff,1);
    FrontMaterial.specular = glm::vec4(spec,1);
    FrontMaterial.shininess = shin*128;
    {
        glBindBuffer( GL_UNIFORM_BUFFER, ubo[1] );
        GLvoid *p = glMapBuffer( GL_UNIFORM_BUFFER, GL_WRITE_ONLY );
        memcpy( p, &FrontMaterial, sizeof(FrontMaterial) );
        glUnmapBuffer( GL_UNIFORM_BUFFER );
    }
    glBindBuffer( GL_UNIFORM_BUFFER, 0 );

    // une théière
    matrModel.PushMatrix();{
        matrModel.Translate( pos.x, pos.y, 0.0 );
        matrModel.Translate( 0, -1, 0 ); // pour mettre l'origine au centre de la théière
        matrModel.Scale( 0.4, 0.4, 0.4 );
        glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
        glUniformMatrix3fv( locmatrNormale, 1, GL_TRUE, glm::value_ptr( glm::inverse( glm::mat3( matrVisu.getMatr() * matrModel.getMatr() ) ) ) );
        theiere->afficher( );
    }matrModel.PopMatrix();
}

void FenetreTP::afficherScene()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glUseProgram( prog );

    // définir le pipeline graphique
    //matrProj.Perspective( 40.0, (GLdouble)largeur_ / (GLdouble)hauteur_, 0.1, 60.0 );
    if ( largeur_ <= hauteur_ )
        matrProj.Ortho( -11.0, 11.0, -11.0*(GLfloat)hauteur_/(GLfloat)largeur_, 11.0*(GLfloat)hauteur_/(GLfloat)largeur_, 0.1, 60.0 );
    else
        matrProj.Ortho( -11.0*(GLfloat)largeur_/(GLfloat)hauteur_, 11.0*(GLfloat)largeur_/(GLfloat)hauteur_, -11.0, 11.0, 0.1, 60.0 );
    glUniformMatrix4fv( locmatrProj, 1, GL_FALSE, matrProj );

    camera.definir();
    glUniformMatrix4fv( locmatrVisu, 1, GL_FALSE, matrVisu );

    matrModel.LoadIdentity();
    glUniformMatrix4fv( locmatrModel, 1, GL_FALSE, matrModel );
    glUniformMatrix3fv( locmatrNormale, 1, GL_TRUE, glm::value_ptr( glm::inverse( glm::mat3( matrVisu.getMatr() * matrModel.getMatr() ) ) ) );

    // mettre à jour les blocs de variables uniformes (ubo)
    {
        glBindBuffer( GL_UNIFORM_BUFFER, ubo[0] );
        GLvoid *p = glMapBuffer( GL_UNIFORM_BUFFER, GL_WRITE_ONLY );
        memcpy( p, &LightSource, sizeof(LightSource) );
        glUnmapBuffer( GL_UNIFORM_BUFFER );
    }
    {
        glBindBuffer( GL_UNIFORM_BUFFER, ubo[1] );
        GLvoid *p = glMapBuffer( GL_UNIFORM_BUFFER, GL_WRITE_ONLY );
        memcpy( p, &FrontMaterial, sizeof(FrontMaterial) );
        glUnmapBuffer( GL_UNIFORM_BUFFER );
    }
    {
        glBindBuffer( GL_UNIFORM_BUFFER, ubo[2] );
        GLvoid *p = glMapBuffer( GL_UNIFORM_BUFFER, GL_WRITE_ONLY );
        memcpy( p, &LightModel, sizeof(LightModel) );
        glUnmapBuffer( GL_UNIFORM_BUFFER );
    }
    glBindBuffer( GL_UNIFORM_BUFFER, 0 );

    // dessiner la scène
    afficherTheiere( glm::vec2( -6.0,  7.5 ), glm::vec3( 0.0215, 0.1745, 0.0215 ),       glm::vec3( 0.07568, 0.61424, 0.07568 ),    glm::vec3( 0.633, 0.727811, 0.633 ),             0.6 );
    afficherTheiere( glm::vec2( -6.0,  4.5 ), glm::vec3( 0.135, 0.2225, 0.1575 ),        glm::vec3( 0.54, 0.89, 0.63 ),             glm::vec3( 0.316228, 0.316228, 0.316228 ),       0.1 );
    afficherTheiere( glm::vec2( -6.0,  1.5 ), glm::vec3( 0.05375, 0.05, 0.06625 ),       glm::vec3( 0.18275, 0.17, 0.22525 ),       glm::vec3( 0.332741, 0.328634, 0.346435 ),       0.3 );
    afficherTheiere( glm::vec2( -6.0, -1.5 ), glm::vec3( 0.25, 0.20725, 0.20725 ),       glm::vec3( 1, 0.829, 0.829 ),              glm::vec3( 0.296648, 0.296648, 0.296648 ),       0.088 );
    afficherTheiere( glm::vec2( -6.0, -4.5 ), glm::vec3( 0.1745, 0.01175, 0.01175 ),     glm::vec3( 0.61424, 0.04136, 0.04136 ),    glm::vec3( 0.727811, 0.626959, 0.626959 ),       0.6 );
    afficherTheiere( glm::vec2( -6.0, -7.5 ), glm::vec3( 0.1, 0.18725, 0.1745 ),         glm::vec3( 0.396, 0.74151, 0.69102 ),      glm::vec3( 0.297254, 0.30829, 0.306678 ),        0.1 );
    afficherTheiere( glm::vec2( -2.0,  7.5 ), glm::vec3( 0.329412, 0.223529, 0.027451 ), glm::vec3( 0.780392, 0.568627, 0.113725 ), glm::vec3( 0.992157, 0.941176, 0.807843 ),       0.21794872 );
    afficherTheiere( glm::vec2( -2.0,  4.5 ), glm::vec3( 0.2125, 0.1275, 0.054 ),        glm::vec3( 0.714, 0.4284, 0.18144 ),       glm::vec3( 0.393548, 0.271906, 0.166721 ),       0.2 );
    afficherTheiere( glm::vec2( -2.0,  1.5 ), glm::vec3( 0.25, 0.25, 0.25 ),             glm::vec3( 0.4, 0.4, 0.4 ),                glm::vec3( 0.774597, 0.774597, 0.774597 ),       0.6 );
    afficherTheiere( glm::vec2( -2.0, -1.5 ), glm::vec3( 0.19125, 0.0735, 0.0225 ),      glm::vec3( 0.7038, 0.27048, 0.0828 ),      glm::vec3( 0.256777, 0.137622, 0.086014 ),       0.1 );
    afficherTheiere( glm::vec2( -2.0, -4.5 ), glm::vec3( 0.24725, 0.1995, 0.0745 ),      glm::vec3( 0.75164, 0.60648, 0.22648 ),    glm::vec3( 0.628281, 0.555802, 0.366065 ),       0.4 );
    afficherTheiere( glm::vec2( -2.0, -7.5 ), glm::vec3( 0.19225, 0.19225, 0.19225 ),    glm::vec3( 0.50754, 0.50754, 0.50754 ),    glm::vec3( 0.508273, 0.508273, 0.508273 ),       0.4 );
    afficherTheiere( glm::vec2(  2.0,  7.5 ), glm::vec3( 0.0, 0.0, 0.0 ),                glm::vec3( 0.01, 0.01, 0.01 ),             glm::vec3( 0.50, 0.50, 0.50 ),                   0.25 );
    afficherTheiere( glm::vec2(  2.0,  4.5 ), glm::vec3( 0.0, 0.1, 0.06 ),               glm::vec3( 0.0, 0.50980392, 0.50980392 ),  glm::vec3( 0.50196078, 0.50196078, 0.50196078 ), 0.25 );
    afficherTheiere( glm::vec2(  2.0,  1.5 ), glm::vec3( 0.0, 0.0, 0.0 ),                glm::vec3( 0.1, 0.35, 0.1 ),               glm::vec3( 0.45, 0.55, 0.45 ),                   0.25 );
    afficherTheiere( glm::vec2(  2.0, -1.5 ), glm::vec3( 0.0, 0.0, 0.0 ),                glm::vec3( 0.5, 0.0, 0.0 ),                glm::vec3( 0.7, 0.6, 0.6 ),                      0.25 );
    afficherTheiere( glm::vec2(  2.0, -4.5 ), glm::vec3( 0.0, 0.0, 0.0 ),                glm::vec3( 0.55, 0.55, 0.55 ),             glm::vec3( 0.70, 0.70, 0.70 ),                   0.25 );
    afficherTheiere( glm::vec2(  2.0, -7.5 ), glm::vec3( 0.0, 0.0, 0.0 ),                glm::vec3( 0.5, 0.5, 0.0 ),                glm::vec3( 0.60, 0.60, 0.50 ),                   0.25 );
    afficherTheiere( glm::vec2(  6.0,  7.5 ), glm::vec3( 0.02, 0.02, 0.02 ),             glm::vec3( 0.01, 0.01, 0.01 ),             glm::vec3( 0.4, 0.4, 0.4 ),                      0.078125 );
    afficherTheiere( glm::vec2(  6.0,  4.5 ), glm::vec3( 0.0, 0.05, 0.05 ),              glm::vec3( 0.4, 0.5, 0.5 ),                glm::vec3( 0.04, 0.7, 0.7 ),                     0.078125 );
    afficherTheiere( glm::vec2(  6.0,  1.5 ), glm::vec3( 0.0, 0.05, 0.0 ),               glm::vec3( 0.4, 0.5, 0.4 ),                glm::vec3( 0.04, 0.7, 0.04 ),                    0.078125 );
    afficherTheiere( glm::vec2(  6.0, -1.5 ), glm::vec3( 0.05, 0.0, 0.0 ),               glm::vec3( 0.5, 0.4, 0.4 ),                glm::vec3( 0.7, 0.04, 0.04 ),                    0.078125 );
    afficherTheiere( glm::vec2(  6.0, -4.5 ), glm::vec3( 0.05, 0.05, 0.05 ),             glm::vec3( 0.5, 0.5, 0.5 ),                glm::vec3( 0.7, 0.7, 0.7 ),                      0.078125 );
    afficherTheiere( glm::vec2(  6.0, -7.5 ), glm::vec3( 0.05, 0.05, 0.0 ),              glm::vec3( 0.5, 0.5, 0.4 ),                glm::vec3( 0.7, 0.7, 0.04 ),                     0.078125 );

    // afficher les axes
    if ( Etat::afficheAxes ) FenetreTP::afficherAxes( 1.0 );

    // FenetreTP::VerifierErreurGL("fin afficherScene");

    // permuter tampons avant et arrière
    swap();
}

void FenetreTP::redimensionner( GLsizei w, GLsizei h )
{
    glViewport( 0, 0, w, h );
}

void FenetreTP::clavier( TP_touche touche )
{
    switch ( touche )
    {
    case TP_ECHAP:
    case TP_q: // Quitter l'application
        quit();
        break;

    case TP_x: // Activer/désactiver l'affichage des axes
        Etat::afficheAxes = !Etat::afficheAxes;
        std::cout << "// Affichage des axes ? " << ( Etat::afficheAxes ? "OUI" : "NON" ) << std::endl;
        break;

    case TP_v: // Recharger les fichiers des nuanceurs et recréer le programme
        chargerNuanceurs();
        std::cout << "// Recharger nuanceurs" << std::endl;
        break;

    case TP_g: // Permuter l'affichage en fil de fer ou plein
        {
            GLint modePlein[2];
            glGetIntegerv( GL_POLYGON_MODE, modePlein );
            glPolygonMode( GL_FRONT_AND_BACK, ( modePlein[0] == GL_LINE ) ? GL_FILL : GL_LINE );
        }
        break;

    case TP_s: // Sauvegarder une copie de la fenêtre dans un fichier
        sauvegarderFenetre( );
        break;

    default:
        std::cout << " touche inconnue : " << (char) touche << std::endl;
        imprimerFichier( "touches.txt" );
        break;
    }
}

// fonction callback pour un clic de souris
glm::ivec2 sourisPosPrec(0,0);
static bool presse = false;
void FenetreTP::sourisClic( int button, int state, int x, int y )
{
    presse = ( state == TP_PRESSE );
    if ( presse )
    {
        sourisPosPrec.x = x;
        sourisPosPrec.y = y;
    }
}

void FenetreTP::sourisMolette( int x, int y )
{
}

// fonction de mouvement de la souris
void FenetreTP::sourisMouvement( int x, int y )
{
    if ( presse )
    {
        int dx = x - sourisPosPrec.x;
        int dy = y - sourisPosPrec.y;
        camera.theta -= dx / 10.0;
        camera.phi   += dy / 10.0;

        sourisPosPrec.x = x;
        sourisPosPrec.y = y;

        camera.verifierAngles();
    }
}

int main( int argc, char *argv[] )
{
    // créer une fenêtre
    FenetreTP fenetre( "theieres" );

    // allouer des ressources et définir le contexte OpenGL
    fenetre.initialiser();

    bool boucler = true;
    while ( boucler )
    {
        // mettre à jour la physique
        calculerPhysique( );

        // affichage
        fenetre.afficherScene();

        // récupérer les événements et appeler la fonction de rappel
        boucler = fenetre.gererEvenement();
    }

    // détruire les ressources OpenGL allouées
    fenetre.conclure();

    return 0;
}
